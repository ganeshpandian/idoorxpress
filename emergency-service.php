<?php include 'header.php'; ?>
	<div class="container-fluid emergency-deisgn">
		<div class="page-comer container">
			Home / Emergency-service
		</div>
		<div class="container list-of-services">
			<h2>Emergency Services</h2>
			<ul>
				<?php for($i = 0; $i < 9; $i ++): ?>
				<li>
					<figure><img src="dist/images/policeman.png"></figure>
					<h3>Police</h3>
					<a href="#"><i class="icon-phone-call"></i> 100</a>
				</li>
				<li>
					<figure><img src="dist/images/firefighter.png"></figure>
					<h3>Fire Man</h3>
					<a href="#"><i class="icon-phone-call"></i> 108</a>
				</li>
			<?php endfor; ?>
			</ul>
		</div>
	</div>

<?php include 'footer.php'; ?>