<?php include 'header.php'; ?>
<div class="prof-banner">

</div>
<div class="add-addr">
    <h2>Your Account</h2>
    <div class="add-account">
        <h3>Account Details</h3>
        <hr>
        <div class="displ-pic">
            <div class="show-image editzz">
                <div id="image-holder"><img src="dist/images/review-img.jpg"></div>
                <label class="hove-edit" for="fileUpload"><i class="icon-edit-3"></i> Edit</label>
                <input id="fileUpload" type="file" name="photo" class="edit"><br>
            </div>
        </div>
        <div class="name-frst">
            <div class="form-area">
                <label>First Name</label>
                <input type="text" name="" id="" placeholder="Enter First Name">
            </div>
            <div class="form-area">
                <label>Last Name</label>
                <input type="text" name="" id="" placeholder="Enter Last Name">
            </div>
        </div>
        <div class="name-frst">
            <div class="form-area">
                <label>Email</label>
                <input type="text" name="" id="" placeholder="Enter Email Address">
            </div>
            <div class="form-area">
                <label>Mobile Number</label>
                <input type="text" name="" id="" placeholder="Enter Mobile Number">
            </div>
        </div>
        <button type="submit" class="save-submit">Save</button>
    </div>
</div>
<?php include 'footer.php'; ?>