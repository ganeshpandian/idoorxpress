<div class="error-head">
	<?php include 'header.php'; ?>
</div>

<div class="container-fluid nooo-page server-error">
	<div class="page-not">
		<h1>500 Internal Server Error</h1>
		<p>Please try again later or feel free to contact us if the problem persists</p>
		<img src="dist/images/500-animate.gif">
	</div>
</div>

<div class="error-foot">
	<?php include 'footer.php'; ?>
</div>
