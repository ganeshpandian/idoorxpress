<?php include 'header-loggedin.php'; ?>
<div class="back-pg">
	<div class="container tasker-desk">
		<h3>Pick a Tasker</h3>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel sagittis massa. Cras sit amet urna velit. Cras rutrum purus sit amet leo dictum tempor. Suspendisse ipsum sapien, dapibus sed sollicitudin quis, egestas non metus. Nulla sed diam eros. Vivamus a pharetra turpis. </p>
		<div class="col-md-4 selet-task">
			<h4>Sorted By</h4>
			<select class="form-control" id="sortby">
		      <option>Recomended</option>
		      <option>Priority</option>
		      <option>Friendly</option>
		      <option>Usually</option>
		      <option>Carelessly</option>
		    </select>
		    <div class="form-sec">
		    	<h4>How Often</h4>
		    	<ul>
		    		<li class="active">Weekly</li>
		    		<li>Every 2 Weeks</li>
		    		<li>Every 4 Weeks</li>
		    		<li>Just Once</li>
		    	</ul>
		    </div>
		    <div class="address-locate">
		  		<h4>Task Date</h4>
		  		<div class="container-fluid owl-carousel tasker-date-select">
		  			<div class="item">
		  				<div class="actives">
		  					<span>14</span><b>Friday</b>
		  				</div>
		  			</div>
		  			<div class="item">
		  				<div class="">
		  					<span>15</span><b>Saturday</b>
		  				</div>
		  			</div>
		  			<div class="item">
		  				<div class="">
		  					<span>16</span><b>Sunday</b>
		  				</div>
		  			</div>
		  			<div class="item">
		  				<div class="">
		  					<span>17</span><b>Monday</b>
		  				</div>
		  			</div>
		  			<div class="item">
		  				<div class="">
		  					<span>18</span><b>Tuesday</b>
		  				</div>
		  			</div>
		  			<div class="item">
		  				<div class="">
		  					<span>19</span><b>Wednesday</b>
		  				</div>
		  			</div>
		  			<div class="item">
		  				<div class="">
		  					<span>20</span><b>Thursday</b>
		  				</div>
		  			</div>
		  		</div>
	      	</div>
	      	<select class="form-control" id="sort_time">
		      <option>I'm Felexible</option>
		      <option>10AM-12PM</option>
		      <option>12PM-2PM</option>
		      <option>2PM-5PM</option>
		      <option>5PM-7PM</option>
		    </select>
		    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel sagittis massa. Cras sit amet urna velit. Cras rutrum purus sit amet leo dictum tempor.</p>
		    <div class="cnt-us">
		    	<h4>Contact Us</h4>
		    	<img src="dist/images/call-center.png">
		    	<button>Connect Now</button>
		    </div>
		</div>
		<div class="col-md-8 selet-person">
			<div class="each-tasker no-tasker">
				<img src="dist/images/no-results.png">
				<h3>This doesn’t usually happen try changing your filters.</h3>
				<h3>No tasker Found</h3>
			</div>
			<?php for($i = 0; $i <5; $i ++): ?>
			<div class="each-tasker">
				<div class="col-md-3 prof-photo">
					<figure><img src="dist/images/review-img.jpg"></figure>
					<ul>
						<li class="active"><img src="dist/images/svg/star.svg" class="svg"></li>
						<li class="active"><img src="dist/images/svg/star.svg" class="svg"></li>
						<li class="active"><img src="dist/images/svg/star.svg" class="svg"></li>
						<li><img src="dist/images/svg/star.svg" class="svg"></li>
						<li><img src="dist/images/svg/star.svg" class="svg"></li>
					</ul>
					<a href="#" class="select-check" data-toggle="modal" data-target="#payment_steps">Select & Continue</a>
					<a href="#" data-toggle="modal" data-target="#review_task">Review & Profile</a>
				</div>
				<div class="col-md-9 tasker-info">
					<span class="pull-right"><b>$32.00/hr</b></span>
					<h3>Aravinth Sakthivel</h3>
					<span><i class="icon-check2"></i> Sever task Completed</span>
					<span><i class="icon-check2"></i> Cleaning Review 100%</span>
					<span><i class="icon-check2"></i> Sever task Completed</span>
					<span><i class="icon-check2"></i> Cleaning Review 100%</span>
					<h4>How can i help:</h4>
					<p>Vestibulum sed nisl sollicitudin, dapibus felis eget, pretium risus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam dictum vulputate libero, eu volutpat velit consequat quis. Suspendisse vel neque eget sem consectetur finibus id nec quam. Duis aliquet augue quis rhoncus gravida. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce laoreet at elit eu ultricies. </p>
				</div>
			</div>
			<?php endfor; ?> 
		</div>
	</div>
</div>
<div class="modal fade" id="payment_steps" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<i class="icon-circle-with-cross"></i>
		</button>
      <div class="proj-method">
      	<h3>Payment Options</h3>
      		<label><input type="radio" id="payment1" name="payment" checked="">Visa/Mastercard</label>
      		<div class="fill-card">
				<span>Please fill your card information...</span>
				<img src="dist/images/aba.png">
				<div class="form-area">
					<label>Card holder name</label>
					<input type="text" placeholder="Card holder name">
				</div>
				<div class="form-area">
					<label>Cedit/Debit Card number</label>
					<input name="credit-number" class="cc-number" type="tel" pattern="\d*" maxlength="19" placeholder="Card Number">
				</div>
				<div class="col-md-12 expiry-cvv">
					<div class="col-md-6 date-expiry">
						<div class="form-area">
							<label>Expiry date</label>
							<span>
								<select class="form-control" id="month_expiry">
									<option>MM</option>
									<option>01</option>
									<option>02</option>
									<option>03</option>
									<option>04</option>
									<option>05</option>
									<option>06</option>
									<option>07</option>
									<option>08</option>
									<option>09</option>
									<option>10</option>
									<option>11</option>
									<option>12</option>
							    </select>
							    <select class="form-control" id="year_expiry">
									<option>YY</option>
									<option>18</option>
									<option>19</option>
									<option>20</option>
									<option>21</option>
									<option>22</option>
									<option>23</option>
							    </select>
							</span>
						</div>
					</div>
					<div class="col-md-6 cvc-cvv">
						<div class="check-cvv">
							<label>CVC/CVV <i class="icon-help-circle"></i></label>
							<input type="text" name="" maxlength="3" placeholder=". . .">
						</div>
					</div>
				</div>
				<div class="form-area">
					<span><input type="checkbox" name=""> Save this card to my account for faster booking</span>
				</div>
				<div class="form-area">
					<label>Email</label>
					<input type="email" name="" placeholder="enter email">
				</div>
			</div>
			<label><input type="radio" id="payment1" name="payment">Cash Collection Service</label>
			<div class="tot-amnt">
			    <h2>Total Amount : $56.70 <b>(show fare breakup)</b></h2>
			    <h5>By clicking on "Pay Now", you agree to the</h5>
				<button>Pay Now</button>
	      </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="review_task" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<i class="icon-circle-with-cross"></i>
		</button>
      <div class="review-method">
      	<h3>Task Review</h3>
      	<div class="task-information">
      		<span>Task Category:</span>
      		<b>2 ton a/c at home in cambodia</b>
      	</div>
      	<div class="task-information">
      		<span>Task Date:</span>
      		<b>26-apr-2019</b>
      	</div>
      	<div class="task-information">
      		<span>Task Time:</span>
      		<b>Í'm Flexible</b>
      	</div>
      	<div class="task-information">
      		<span>Task Details:</span>
      		<b>Near River Side</b>
      	</div>
      	<div class="task-information">
      		<span>Task Location:</span>
      		<b>Street 360 near whatt phnom, phnom penh- cambodia</b>
      	</div>
      </div>
      <div class="tasker-prof">
      	<h3>Tasker Profile</h3>
      	<div class="task-info">
      		<figure><img src="dist/images/review-img.jpg"></figure>
      		<span class="pull-right">$40</span>
      		<h4>Rajini Sivaji</h4>
      		<p>
      			<i class="icon-info"></i>
      			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in fringilla ipsum. Morbi libero arcu, feugiat vel justo et, ullamcorper posuere ante. Ut nec rhoncus felis.
      		</p>
      	</div>
      	<input type="submit" name="" value="Confirm">
      </div>
    </div>
  </div>
</div>
<?php include 'footer.php'; ?>