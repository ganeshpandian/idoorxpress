<?php include 'header.php'; ?>

<div class="container">
	<h1>iDoorExpress pages</h1>
	<ul>
		<li>
			<h2>FrontEnd</h2>
			<ul>
				<li><a href="home.php" target="_blank">Home Page</a></li>
				<li><a href="listing.php" target="_blank">Listing Page</a></li>
				<li><a href="become-professional.php" target="_blank">Become a Professional</a></li>
				<li><a href="tasker-confirm.php" target="_blank">Tasker Confirm</a></li>
				<li><a href="404-page.php" target="_blank">404 Page</a></li>
				<li><a href="500-page.php" target="_blank">500 Page</a></li>
				<li><a href="tasker-dashboard.php" target="_blank">Tasker Dashboard</a></li>
				<li><a href="blog-main.php" target="_blank">Blog Page</a></li>
				<li><a href="blog-listing.php" target="_blank">Blog Lisiting</a></li>
				<li><a href="blog-detail.php" target="_blank">Blog Detail Page</a></li>
				<li><a href="about.php" target="_blank">About us Page</a></li>
				<li><a href="contact.php" target="_blank">Contact us Page</a></li>
				<li><a href="play-store.php" target="_blank">Play Store Page</a></li>
				<li><a href="emergency-service.php" target="_blank">Emergency Service</a></li>
				<li><a href="details.php" target="_blank">Details</a></li>
			</ul>
		</li>
		<li>
			<h2>Backend</h2>
			<ul>
				<li><a href="userdashboard.php" target="_blank">User Dashboard</a></li>
			</ul>
		</li>
	</ul>
</div>

<?php include 'footer.php'; ?>