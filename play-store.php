<?php include 'header.php'; ?>
<div class="container-fluid store-area">
	<div class="container google-play">
		<div class="col-md-6 mob-case">
			<img src="dist/images/ios-home.png">
		</div>
		<div class="col-md-6 custom-link">
			<h1>Never miss a new customer.</h1>
			<p>Download the free Thumbtack Pro app and get notified the second someone is interested.</p>
			<a href="#"><img src="dist/images/app_store.png"></a><a href="#"><img src="dist/images/google_play.png"></a>
		</div>
	</div>
</div>
<div class="container-fluid add-footernew">
	<div class="container">
		<h3>RELATED SERVICES IN DELHI</h3>
		<ul>
			<li><a href="#">Architect</a></li>
			<li><a href="#">Carpenter</a></li>
			<li><a href="#">Construction and Renovation</a></li>
			<li><a href="#">Electrician</a></li>
			<li><a href="#">Interior Designer</a></li>
			<li><a href="#">Massage for Men</a></li>
			<li><a href="#">Packers &amp; Movers</a></li>
			<li><a href="#">Hairstyling &amp; Makeup</a></li>
			<li><a href="#">Pest Control</a></li>
			<li><a href="#">Plumber</a></li>
			<li><a href="#">Bathroom Deep Cleaning</a></li>
			<li><a href="#">Home Deep Cleaning</a></li>
			<li><a href="#">Kitchen Deep Cleaning</a></li>
			<li><a href="#">Sofa Cleaning</a></li>
			<li><a href="#">RO or Water Purifier Repair</a></li>
			<li><a href="#">Salon at Home</a></li>
			<li><a href="#">Spa at Home for Women</a></li>
		</ul>
	</div>
</div>

<?php include 'footer.php'; ?>