<?php include 'header-loggedin.php'; ?>
<div class="container dash-board">
	<div class="col-md-3 menu-dash">
		<div class="tab-sec">
			<ul>
				<li class="active"><a href="#vtab1" data-toggle="tab">My Booking</a></li>
				<li><a href="#vtab2" data-toggle="tab">My Account</a></li>
				<li><a href="#vtab3" data-toggle="tab">Notification</a></li>
				<li><a href="#vtab4" data-toggle="tab">Privacy</a></li>
				<li><a href="#vtab5" data-toggle="tab">Security</a></li>
        <li><a href="#vtab7" data-toggle="tab">Inbox</a></li>
				<li><a href="#vtab6" data-toggle="tab">Account Deactivate</a></li>
        <li><a href="#vtab8" data-toggle="tab">My Wallet</a></li>
			</ul>
    	</div>
	</div>
	<div class="col-md-9 content-dash">
		<div class="tab-content">
      <div class="tab-pane active" id="vtab1">
        <ul class="nav nav-tabs">
				  <li class="active"><a data-toggle="tab" href="#all" aria-expanded="true">All</a></li>
				  <li><a data-toggle="tab" href="#awaiting_payment" aria-expanded="true">Awaiting Task</a></li>
				  <li><a data-toggle="tab" href="#upcoming" aria-expanded="true">Upcoming</a></li>
				  <li><a data-toggle="tab" href="#completed" aria-expanded="true">Completed</a></li>
				  <li><a data-toggle="tab" href="#canceled" aria-expanded="true">Canceled</a></li>
				</ul>
				<div class="tab-content">
  					<div id="all" class="tab-pane fade in active">
  						<table> 
                  <thead> 
                      <tr>  
                          <th>Name</th>
                          <th>Reference Number</th>
                          <th>Task Type</th>
                          <th>Date</th>
                          <th>Time</th>
                          <th>Task Status</th>
                      </tr>
                  </thead>
                  <tbody> 
                      <tr>  
                          <td>
                            <figure>
                              <img src="dist/images/review-img.jpg"></figure>
                              Aravinth Sakthivel <br>
                              <span>House Cleaning</span>    
                          </td>
                          <td data-toggle="modal" data-target="#tasker_detail">#R1597536842</td>
                          <td>Laprop Service</td>
                          <td>25 December 2018</td>
                          <td>10:00 AM</td>
                          <td class="status-btn await"><span>Awaiting</span></td>
                      </tr>
                      <tr>  
                          <td>
                            <figure>
                              <img src="dist/images/review-img.jpg"></figure>
                              Aravinth Sakthivel <br>
                              <span>House Cleaning</span>    
                          </td>
                          <td>#R1597536842</td>
                          <td>Laprop Service</td>
                          <td>25 December 2018</td>
                          <td>10:00 AM</td>
                          <td class="status-btn upcome"><span>Upcoming</span></td>
                      </tr>
                      <tr>  
                          <td>
                            <figure>
                              <img src="dist/images/review-img.jpg"></figure>
                              Aravinth Sakthivel <br>
                              <span>House Cleaning</span>    
                          </td>
                          <td>#R1597536842</td>
                          <td>Laprop Service</td>
                          <td>25 December 2018</td>
                          <td>10:00 AM</td>
                          <td class="status-btn comple"><span>Completed</span></td>
                      </tr>
                      <tr>  
                          <td>
                            <figure>
                              <img src="dist/images/review-img.jpg"></figure>
                              Aravinth Sakthivel <br>
                              <span>House Cleaning</span>    
                          </td>
                          <td>#R1597536842</td>
                          <td>Laprop Service</td>
                          <td>25 December 2018</td>
                          <td>10:00 AM</td>
                          <td class="status-btn cancel"><span>Cancel</span></td>
                      </tr>
                      <?php for($i = 0; $i < 9; $i ++): ?>
                      <tr>  
                          <td>
                            <figure>
                              <img src="dist/images/review-img.jpg"></figure>
                              Aravinth Sakthivel <br>
                              <span>House Cleaning</span>    
                          </td>
                          <td>#R1597536842</td>
                          <td>Laprop Service</td>
                          <td>25 December 2018</td>
                          <td>10:00 AM</td>
                          <td class="status-btn upcome"><span>Upcoming</span></td>
                      </tr>
                      <?php endfor; ?>
                  </tbody>
              </table>
  					</div>
  					<div id="awaiting_payment" class="tab-pane fade">
              <div class="no-data">
                <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h2>
                <ul>
                  <li> Nulla quis lorem placerat, pretium purus eget, efficitur mauris.</li>
                  <li> Nulla quis lorem placerat, pretium purus eget, efficitur mauris.</li>
                  <li> Nulla quis lorem placerat, pretium purus eget, efficitur mauris.</li>
                  <li> Nulla quis lorem placerat, pretium purus eget, efficitur mauris.</li>
                  <li> Nulla quis lorem placerat, pretium purus eget, efficitur mauris.</li>
                  <li> Nulla quis lorem placerat, pretium purus eget, efficitur mauris.</li>
                  <li> Nulla quis lorem placerat, pretium purus eget, efficitur mauris.</li>
                  <li> Nulla quis lorem placerat, pretium purus eget, efficitur mauris.</li>
                  <li> Nulla quis lorem placerat, pretium purus eget, efficitur mauris.</li>
                </ul>
              </div>
  						<table> 
                  <thead> 
                      <tr>  
                          <th>Reference Number</th>
                          <th>Task Type</th>
                          <th>Date</th>
                          <th>Time</th>
                          <th>Task Status</th>
                      </tr>
                  </thead>
                  <tbody> 
                      <tr>  
                          <td>#R1597536842</td>
                          <td>Laprop Service</td>
                          <td>25 December 2018</td>
                          <td>10:00 AM</td>
                          <td class="status-btn await"><span>Awaiting</span></td>
                      </tr>
                  </tbody>
              </table>
  					</div>
  					<div id="upcoming" class="tab-pane fade">
  						<table> 
                  <thead> 
                      <tr>  
                          <th>Reference Number</th>
                          <th>Task Type</th>
                          <th>Date</th>
                          <th>Time</th>
                          <th>Task Status</th>
                      </tr>
                  </thead>
                  <tbody> 
                      <tr>  
                          <td>#R1597536842</td>
                          <td>Laprop Service</td>
                          <td>25 December 2018</td>
                          <td>10:00 AM</td>
                          <td class="status-btn upcome" data-toggle="modal" data-target="#confirm_taskservice"><span>Upcoming</span></td>
                      </tr>
                  </tbody>
              </table>
  					</div>
  					<div id="completed" class="tab-pane fade">
  						<table> 
                  <thead> 
                      <tr>  
                          <th>Reference Number</th>
                          <th>Task Type</th>
                          <th>Date</th>
                          <th>Time</th>
                          <th>Task Status</th>
                      </tr>
                  </thead>
                  <tbody> 
                      <tr>  
                          <td>#R1597536842</td>
                          <td>Laprop Service</td>
                          <td>25 December 2018</td>
                          <td>10:00 AM</td>
                          <td class="status-btn comple"><span>Completed</span></td>
                      </tr>
                  </tbody>
              </table>
  					</div>
  					<div id="canceled" class="tab-pane fade">
  						<table> 
                  <thead> 
                      <tr>  
                          <th>Reference Number</th>
                          <th>Task Type</th>
                          <th>Date</th>
                          <th>Time</th>
                          <th>Task Status</th>
                      </tr>
                  </thead>
                  <tbody> 
                      <tr>  
                          <td>#R1597536842</td>
                          <td>Laprop Service</td>
                          <td>25 December 2018</td>
                          <td>10:00 AM</td>
                          <td class="status-btn cancel"><span>Cancel</span></td>
                      </tr>
                  </tbody>
              </table>
  					</div>
  				</div>
        	</div>
        	<div class="tab-pane" id="vtab2">
        		<div class="my-acc">
              <h3>My Account</h3>
              <div class="displ-pic">
                <figure style="    background: linear-gradient(to right, #47c9c9 5%, #81cf8b 100%);"></figure>
                <div class="show-image editzz">
                  <div id="image-holder"><img src="dist/images/review-img.jpg"></div>
                  <label class="hove-edit" for="fileUpload"><i class="icon-edit-3"></i> Edit</label>
                  <input id="fileUpload" type="file" name="photo" class="edit"><br>
                </div>
                <h4>Aravinth Sakthivel</h4>
              </div>
              <div class="service-type-name">
                <img src="dist/images/svg/air.svg" class="svg">
                <h2>Appliance repair</h2>
              </div>
              <div class="pers-info">
                <h5>Personal Information</h5>
                <ul class="dividion-too">
                  <li>
                    <label><h5>First Name</h5> </label>
                    <input type="text" name="firstname" id="firstname">
                    <i class="icon-user"></i>
                  </li>
                  <li>
                    <label><h5>Last Name</h5> </label>
                    <input type="text" name="lastname" id="lastname">
                    <i class="icon-user"></i>
                  </li>
                  <li>
                    <label><h5>Email Address</h5> </label>
                    <input type="text" name="email" id="email">
                    <i class="icon-mail"></i>
                  </li>
                  <li>
                    <label><h5>Phone</h5> </label>
                    <input type="number" name="phone" id="phone">
                    <i class="icon-phone"></i>
                  </li>
                  <li>
                    <label><h5>Gender</h5> </label>
                    <select class="form-control" id="gender">
                      <option>Male</option>
                      <option>Female</option>
                    </select>
                    <i class="fa fa-venus-mars" aria-hidden="true"></i>
                  </li>
                  <li>
                    <label><h5>Date Of Birth</h5></label>
                    <input type='text' class="form-control" id='datetimepicker1' placeholder="10/02/2018" />
                    <i class="icon-calendar2"></i>
                  </li>
                </ul>
                <ul class="dividion-one">
                  <li>
                    <label><h5>About</h5></label>
                    <textarea id="textarea" rows="9" cols="50" maxlength="500"></textarea>
                    <div id="textarea_feedback"></div>
                  </li>
                  <li>
                    <label><h5>Address Line 1</h5></label>
                    <input type="text" name="addresss-1" id="address_1">
                    <i class="icon-send"></i>
                  </li>
                  <li>
                    <label><h5>Address Line 2</h5></label>
                    <input type="text" name="addresss-2" id="address_2">
                    <i class="icon-send"></i>
                  </li>   
                </ul>
                <ul class="dividion-treo">
                  <li>
                    <label><h5>City</h5></label>
                    <input type="text" name="city" id="city">
                    <i class="icon-send"></i>
                  </li>
                  <li>
                    <label><h5>Country</h5></label>
                    <input type="text" name="Country" id="Country">
                    <i class="icon-send"></i>
                  </li>
                  <li>
                    <label><h5>Postal/Zip Code</h5></label>
                    <input type="text" name="zips" id="zips">
                    <i class="icon-send"></i>
                  </li>
                </ul>
                <input type="button" name="" value="Save">
              </div>
            </div>
        	</div>
          <div class="tab-pane" id="vtab3">
            <h3>Notification</h3>
            <?php for($i = 0; $i < 20; $i ++): ?>
            <div class="Notifi-sec">
              <div class="user-image">
                  <figure><img src="dist/images/review-img.jpg"></figure>
              </div>
              <div class="Notifi-cnt">
                  <h4><b>Aravinth Sakthivel</b> has liked the recent property added</h4>
                  <p><i class="icon-clock"></i>3 Hours ago</p>

                  <div class="input-group-btn" id="Notifi_drops"> 
                      <button class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                         <a href="#"><i class="icon-dots-three-vertical"></i></a>
                      </button>
                      <div class="dropdown-menu Notifi-dele showin">  
                        <div><a href="#">Remove</a></div> 
                        <div><a href="#">Delete</a></div> 
                      </div>
                  </div>
              </div>
           </div>
           <?php endfor; ?>
          </div>
          <div class="tab-pane" id="vtab4">
            <h3>Privacy</h3>
          </div>
          <div class="tab-pane" id="vtab5">
            <h3>Security</h3>
            <ul>
              <li><label><h3>Old Password</h3> 
                  <input type="Password"></label></li>
              <li><label><h3>New Password</h3> 
                  <input type="Password"></label></li>
              <li><label><h3>Confirm Password</h3> 
                  <input type="Password"></label></li>
              <li>
                  <input type="submit" class="button button-green" value="Update Password">
              </li>
            </ul>
          </div>
          <div class="tab-pane" id="vtab7">
            <div class="entiore-chats">
             <div class="user-chat">
              <form>
                <label><i class="icon-search"></i>
                <input type="text" name="search" placeholder="Search.....""></label>
              </form>
                <div class="messe-seperate">
                  <div class="overall-chats active">
                    <div class="chat-image active">
                      <figure><img src="dist/images/review-img.jpg"></figure>
                    </div>
                    <div class="chat-cnt">
                      <p><b>Ramachandran Playboy</b></p>
                      <i>1 month ago</i>
                        <div class="input-group-btn" id="chat-drops"> 
                            <button class="btn dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                               <a href="#"><i class="icon-dots-three-vertical"></i></a>
                            </button>
                            <div class="dropdown-menu" id="down-drops">  
                              <div>Edit</div> 
                              <div>Clear Chat</div> 
                              <div>Delete</div> 
                            </div>
                        </div>
                      <p>You: Recent message to be displayed here</p>
                    </div>
                   </div>
                  <?php for($i = 0; $i < 15; $i ++): ?>
                  <div class="overall-chats">
                    <div class="chat-image active">
                      <figure><img src="dist/images/review-img.jpg"></figure>
                    </div>
                    <div class="chat-cnt">
                      <p><b>Ramachandran Playboy</b></p>
                      <i>1 month ago</i>
                        <div class="input-group-btn" id="chat-drops"> 
                            <button class="btn dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                               <a href="#"><i class="icon-dots-three-vertical"></i></a>
                            </button>
                            <div class="dropdown-menu" id="down-drops">  
                              <div>Edit</div> 
                              <div>Clear Chat</div> 
                              <div>Delete</div> 
                            </div>
                        </div>
                      <p>You: Recent message to be displayed here</p>
                    </div>
                   </div>
                  <?php endfor; ?> 
                  <div class="overall-chats">
                    <div class="chat-image">
                      <figure><img src="dist/images/review-img.jpg"></figure>
                    </div>
                    <div class="chat-cnt">
                      <p><b>Ramachandran Playboy</b></p>
                      <i>1 month ago</i>
                        <div class="input-group-btn" id="chat-drops"> 
                            <button class="btn dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                               <a href="#"><i class="icon-dots-three-vertical"></i></a>
                            </button>
                            <div class="dropdown-menu" id="down-drops">  
                              <div>Edit</div> 
                              <div>Clear Chat</div> 
                              <div>Delete</div> 
                            </div>
                        </div>
                      <p>You: Recent message to be displayed here</p>
                    </div>
                   </div>
                 </div>
               </div>
               <div class="chatuser-cnt">
                <div class="prop-cha">
                  <i class="icon-arrow-left" style="display: none;"></i>
                  <figure><img src="dist/images/review-img.jpg"></figure>
                  <span>
                    <h3>Chat Person Name</h3>
                  </span>
                </div>
                <div class="chat-fully" id="scroll_bottom">
                  <?php for($i = 0; $i < 15; $i ++): ?>
                  <div class="message-types">
                    <div class="recived-msg">
                      <figure><img src="dist/images/review-img.jpg"></figure>
                      <i>9.00AM</i>
                      <p class="typed-text"><span>The message received from the other end</span></p>
                    </div>
                    <div class="sent-msg">
                      <p class="sent-text"><span>The message sent from our end</span></p>
                      <i>9.00AM</i>
                    </div>
                  </div>
                  <?php endfor; ?> 
                </div>
                <div class="send-message">
                        <div class="message-text">
                            <textarea style="max-height: 200px;" class="textarea_auto" placeholder="Type your message here....."></textarea>
                        </div>
                        <div class="send-button" data-toggle="tooltip" title="Send">
                          <button type="submit" id="submit">
                            
                        </button>
                            
                        </div>
                    </div>
               </div>
            </div>
          </div>
          <div class="tab-pane" id="vtab6">
            <h3>Account Deactivate</h3>
            <div class="deactive-area">
              <p>Remove your Account Permanently.</p>
              <span>
              <h4>Remove your Account</h4>
              <p><u>Alert:</u>If you delete your account you will be unsubscribe all the activities and articles then you will be loss skytuber access forever.</p>
              </span>
               <a href="#" data-toggle="modal" data-target="#skyalert" class="button button-green">Delete</a>
                <div class="modal fade" id="skyalert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document" id="skyees">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Delete Your Account</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i class="icon-cross"></i>
                          </button>
                        </div>
                        <div class="modal-body">
                          <p>Are you sure you want to delete your account  permanently!</p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-primary button-green" data-dismiss="modal">Delete</button>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
          </div>
          <div class="tab-pane" id="vtab8">
            <h3>My Wallet</h3>
            <div class="request-depost">
              <button>
                <img src="dist/images/deposit.png">
                <h4>Request to Deposit</h4>
              </button>
              <button>
                <img src="dist/images/withdraw.png">
                <h4>Request to Withdraw</h4>
              </button>
            </div>
            <div class="deposit-amount">
              <h4>Select-bank</h4>
              <ul>
                <li class="dropdown cover-bank"><a href="#" data-toggle="dropdown">
                  <figure><img src="dist/images/aba.png"></figure> ABA Bank
                 <i class="icon-chevron-down"></i>
               </a>
                  <ul class="dropdown-menu" aria-labelledby="dLabel">
                    <li><a href="#"><figure><img src="dist/images/aba.png"></figure> ABA Bank</a></li>
                    <li><a href="#"><figure><img src="dist/images/aba.png"></figure> ABA Bank</a></li>
                    <li><a href="#"><figure><img src="dist/images/aba.png"></figure> ABA Bank</a></li>
                    <li><a href="#"><figure><img src="dist/images/aba.png"></figure> ABA Bank</a></li>
                    <li><a href="#"><figure><img src="dist/images/aba.png"></figure> ABA Bank</a></li>
                    <li><a href="#"><figure><img src="dist/images/aba.png"></figure> ABA Bank</a></li>
                  </ul>
                </li>
                <li class="input-segment">
                  <div class="form-group">
                      <input type="text" id="acc_name" class="form-control" required="">
                      <label class="form-control-placeholder" for="acc_name"><b>*</b>Name</label>
                  </div>
                </li>
                <li class="input-segment">
                  <div class="form-group">
                      <input type="text" id="acc_num" class="form-control" required="">
                      <label class="form-control-placeholder" for="acc_num"><b>*</b>Account Number</label>
                  </div>
                </li>
                <li class="input-segment">
                  <div class="form-group">
                      <input type="text" id="acc_amount" class="form-control" required="">
                      <label class="form-control-placeholder" for="acc_amount"><b>*</b>Amount</label>
                  </div>
                </li>
                <li>
                  <a href="#" class="choose_bank">Select</a>
                </li>
              </ul>
            </div>
            <div class="wallet-show">
              <table> 
                <thead> 
                    <tr>  
                        <th>Select</th>
                        <th>Name</th>
                        <th>Account Number</th>
                        <th>Amount Deposited</th>
                        <th>Task Status</th>
                    </tr>
                </thead>
                <tbody> 
                    <?php for($i = 0; $i < 9; $i ++): ?>
                    <tr>  
                        <td><input type="radio" name=""></td>
                        <td>Aravinth Sakthivel</td>
                        <td>98523698741256</td>
                        <td>$1500</td>
                        <td><a href="#">Select</a></td>
                    </tr>
                    <?php endfor; ?>
                </tbody>
            </table>
            </div>
          </div>
        </div>
	</div>
</div>

<?php include 'footer.php'; ?>

<div class="modal" id="confirm_services" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
  <div class="services-list">
    <a href="#" class="desktop-hide"><i class="icon-cross"></i></a>
    <div class="col-md-5 name-service">
        <div class="nav vab-label" id="v-pills-tab" role="tablist" aria-orientation="vertical">
          <a class="nav-link active" data-toggle="pill" href="#app_repair" role="tab" aria-selected="true">
            <img src="dist/images/svg/air.svg" class="svg">
            <h6>Online</h6>
          </a>
          <a class="nav-link" data-toggle="pill" href="#beauty_spa" role="tab" aria-selected="false">
            <img src="dist/images/svg/heart.svg" class="svg">
            <h6>Offline</h6>
          </a>
        </div>
      </div>
      <div class="col-md-7 select-service">
        <div class="tab-content" id="v-pills-tabContent">
        <div class="tab-pane fade active in" id="app_repair" role="tabpanel">
          <div class="list-of-services">
            <div class="form-area">
              <label>
                <input type="radio" name="">
                <img src="dist/images/pipay.png">
              </label>
            </div>
            <div class="form-area">
              <label>
                <input type="radio" name="">
                <img src="dist/images/aba-bank.png">
              </label>
            </div>
            <div class="form-area">
              <label>
                <input type="radio" name="">
                <img src="dist/images/wing_new.png">
              </label>
            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="beauty_spa" role="tabpanel">
          <div class="list-of-services">
            <img src="dist/images/cashier.png">
            <p>Once the Service is completed, kindly hand it over the cash to concern person who is attending your Services.</p>
          </div>
        </div>
        </div>
      </div>
      <div class="tasker-prof">
        <h3>Tasker Profile</h3>
        <div class="task-info">
          <figure><img src="dist/images/review-img.jpg"></figure>
          <span class="pull-right">$40</span>
          <h4>Rajini Sivaji</h4>
          <p>
            <i class="icon-info"></i>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in fringilla ipsum. Morbi libero arcu, feugiat vel justo et, ullamcorper posuere ante. Ut nec rhoncus felis.
          </p>
        </div>
        <input type="submit" name="" value="Pay Now">
      </div>
  </div>
  </div>
</div>


<div class="modal" id="confirm_taskservice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="service-pay">
      <div class="service-provider">
        <figure><img src="dist/images/review-img.jpg"></figure>
        <h4>Rajini Sivaji</h4>
        <p>2 ton A/C</p>
        <h2>$20</h2>
      </div>
      <div class="pay-type">
        <p>Your Task has been Completed</p>
        <button type="button">Pay by Cash</button><button type="button">Pay by Online</button>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="tasker_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <i class="icon-circle-with-cross"></i>
    </button>
      <div class="review-method">
        <h3>Task Review</h3>
        <div class="pull-right dis-pic">
          <figure><img src="dist/images/review-img.jpg"></figure>
        </div>
        <div class="task-information">
          <span>Task Category:</span>
          <b>2 ton a/c at home in cambodia</b>
        </div>
        <div class="task-information">
          <span>Task Date:</span>
          <b>26-apr-2019</b>
        </div>
        <div class="task-information">
          <span>Task Time:</span>
          <b>Í'm Flexible</b>
        </div>
        <div class="task-information">
          <span>Task Details:</span>
          <b>Near River Side</b>
        </div>
        <div class="task-information">
          <span>Task Location:</span>
          <b>Street 360 near whatt phnom, phnom penh- cambodia</b>
        </div>
      </div>
      <div class="descrip-view">
        <a class="btn" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">Description</a>
        <div class="collapse" id="collapseExample">
          <ul>
            <li>Anim pariatur cliche reprehenderit, enim eiusmod high</li>
            <li>Anim pariatur cliche reprehenderit, enim eiusmod high</li>
            <li>Anim pariatur cliche reprehenderit, enim eiusmod high</li>
            <li>Anim pariatur cliche reprehenderit, enim eiusmod high</li>
            <li>Anim pariatur cliche reprehenderit, enim eiusmod high</li>
          </ul>
        </div>
      </div>
      <div class="tasker-prof">
        <h3>Tasker Profile</h3>
        <div class="task-info">
          <figure><img src="dist/images/review-img.jpg"></figure>
          <span class="pull-right rate-star">
            <i class="icon-star2"></i>4.5
          </span>
          <h4>Rajini Sivaji</h4>
          <p>
            <i class="icon-info"></i>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in fringilla ipsum. Morbi libero arcu, feugiat vel justo et, ullamcorper posuere ante. Ut nec rhoncus felis.
          </p>
          <main class="pull-left">Last hired: 2Days Ago</main>
          <main class="pull-right">Last hired: 2Days Ago</main>
        </div>
        <div class="task-bill">
          <span>SubTotal: 20$</span>          
          <span>Total: 120$</span>          
        </div>
        <input type="submit" name="" value="Confirm">
      </div>
    </div>
  </div>
</div>