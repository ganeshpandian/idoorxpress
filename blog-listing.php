<?php include 'header-loggedin.php'; ?>
	<div class="container-fluid cover-space">
		<div class="container inspire-you">
			<h3>Find your inspiration</h3>
			<label>
				<i class="icon-search"></i>
				<input type="text" name="" placeholder="Discover a Place">
			</label>
		</div>
	</div>
	
	<div class="blog-page container">
		<div class="main-third-area">
			<ul>
				<?php for($i = 0; $i < 5; $i ++): ?>
				<li>
					<figure class="video-seg"><img src="dist/images/blog-3.jpg"><i class="icon-controller-play"></i></figure>
					<span class="pull-right"><i class="icon-heart-outlined"></i></span>
					<div class="text-head">
						<a href="#">
							<b>Thailand</b>
							<h3>These Spirit Houses In Thailand May Save Your Life</h3>
						</a>
					</div>
				</li>
				<li>
					<figure><img src="dist/images/blog-4.jpg"></figure>
					<span class="pull-right"><i class="icon-heart-outlined"></i></span>
					<div class="text-head">
						<a href="#">
							<b>Thailand</b>
							<h3>These Spirit Houses In Thailand May Save Your Life</h3>
						</a>
					</div>
				</li>
				<li>
					<figure><img src="dist/images/blog-5.jpg"></figure>
					<span class="pull-right"><i class="icon-heart-outlined"></i></span>
					<div class="text-head">
						<a href="#">
							<b>Thailand</b>
							<h3>These Spirit Houses In Thailand May Save Your Life</h3>
						</a>
					</div>
				</li>
				<?php endfor; ?>
			</ul>
		</div>
	</div>
<?php include 'footer.php'; ?>