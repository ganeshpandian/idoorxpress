<?php include 'header.php'; ?>

<div class="container">
	<div class="modal fade in bill-details" id="tasker_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: block;">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="review-method">
	        <h3>Task Review</h3>
	        <div class="pull-right dis-pic">
	          <figure><img src="dist/images/review-img.jpg"></figure>
	        </div>
	        <div class="task-information">
	          <span>Task Category:</span>
	          <b>2 ton a/c at home in cambodia</b>
	        </div>
	        <div class="task-information">
	          <span>Task Date:</span>
	          <b>26-apr-2019</b>
	        </div>
	        <div class="task-information">
	          <span>Task Time:</span>
	          <b>Í'm Flexible</b>
	        </div>
	        <div class="task-information">
	          <span>Task Details:</span>
	          <b>Near River Side</b>
	        </div>
	        <div class="task-information">
	          <span>Task Location:</span>
	          <b>Street 360 near whatt phnom, phnom penh- cambodia</b>
	        </div>
	      </div>
	      <div class="descrip-view">
	        <a class="btn" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">Description</a>
	        <div class="collapse" id="collapseExample">
	          <ul>
	            <li>Anim pariatur cliche reprehenderit, enim eiusmod high</li>
	            <li>Anim pariatur cliche reprehenderit, enim eiusmod high</li>
	            <li>Anim pariatur cliche reprehenderit, enim eiusmod high</li>
	            <li>Anim pariatur cliche reprehenderit, enim eiusmod high</li>
	            <li>Anim pariatur cliche reprehenderit, enim eiusmod high</li>
	          </ul>
	        </div>
	      </div>
	      <div class="tasker-prof">
	        <h3>Tasker Profile</h3>
	        <div class="task-info">
	          <figure><img src="dist/images/review-img.jpg"></figure>
	          <span class="pull-right rate-star">
	            <i class="icon-star2"></i>4.5
	          </span>
	          <h4>Rajini Sivaji</h4>
	          <p>
	            <i class="icon-info"></i>
	            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in fringilla ipsum. Morbi libero arcu, feugiat vel justo et, ullamcorper posuere ante. Ut nec rhoncus felis.
	          </p>
	          <main class="pull-left">Last hired: 2Days Ago</main>
	          <main class="pull-right">Last hired: 2Days Ago</main>
	        </div>
	        <div class="task-bill">
	          <span>SubTotal: 20$</span>          
	          <span>Total: 120$</span>          
	        </div>
	        <input type="submit" name="" value="Confirm">
	      </div>
	    </div>
	  </div>
	</div>
</div>

<?php include 'footer.php'; ?>