jQuery(document).ready(function ($) {
    $('.vab-label a').click(function () {
        $('.vab-label a.active').removeClass('active');
        $(this).addClass('active');
    });
    $(".desktop-hide").click(function () {
        $("#show_services").modal('toggle');
    });
    $('#explore_blog_slider').owlCarousel({
        loop: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
                margin: 10,
                nav: false,
                loop: true,
                dots: true,
                autoplay: false
            },
            1023: {
                items: 4,
                margin: 20,
                nav: true,
                loop: false
            }
        }
    });
    $('.date-select').owlCarousel({
        loop: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 3,
                nav: true,
                loop: true,
                dots: true,
                autoplay: true
            },
            520: {
                items: 2,
                nav: false,
                loop: false
            },
            1023: {
                items: 5,
                margin: 20,
                nav: true,
                loop: false
            }
        }
    });
    $('.tasker-date-select').owlCarousel({
        loop: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false,
                loop: true,
                dots: true,
                autoplay: true
            },
            520: {
                items: 2,
                nav: false,
                loop: false
            },
            1023: {
                items: 3,
                margin: 20,
                nav: true,
                loop: false
            }
        }
    });
    $('.owl-carousel').owlCarousel({
        loop: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true,
                loop: false,
                dots: true,
                autoplay: true
            },
            520: {
                items: 2,
                nav: false,
                loop: false
            },
            1023: {
                items: 4,
                margin: 20,
                nav: true,
                loop: false
            }
        }
    });
    $('.owl-carousel.listing-carousel').owlCarousel({
        loop: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false,
                loop: true,
                dots: true,
                autoplay: true
            },
            520: {
                items: 2,
                nav: false,
                loop: false
            },
            1023: {
                items: 4,
                margin: 20,
                nav: true,
                loop: false
            }
        }
    });
});

jQuery('img.svg').each(function () {
    var $img = jQuery(this);
    var imgID = $img.attr('id');
    var imgClass = $img.attr('class');
    var imgURL = $img.attr('src');

    jQuery.get(imgURL, function (data) {
        // Get the SVG tag, ignore the rest
        var $svg = jQuery(data).find('svg');

        // Add replaced image's ID to the new SVG
        if (typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID);
        }
        // Add replaced image's classes to the new SVG
        if (typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass + ' replaced-svg');
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr('xmlns:a');

        // Replace image with new SVG
        $img.replaceWith($svg);

    }, 'xml');
});

//////////////////// Parallex menu ////////////////////////////
$('#mainNav a[href^="#"]').on('click', function (event) {
    var target = $(this.getAttribute('href'));

    if (target.length) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top - 135
        }, 1000);
    }
});
$(window).scroll(function () {
    var scrollDistance = $(window).scrollTop();
    $('.page-section').each(function (i) {
        if ($(this).position().top - 180 <= scrollDistance) {
            $('.navigation a.active').removeClass('active');
            $('.navigation a').eq(i).addClass('active');
        }
    });
}).scroll();
//////////////////// Parallex menu ////////////////////////////
jQuery(document).ready(function ($) {
    var showChar = 150;
    var ellipsestext = "...";
    var moretext = "Show more";
    var lesstext = "Show less";
    $('.mores').each(function () {
        var content = $(this).html();
        if (content.length > showChar) {
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
            var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
            $(this).html(html);
        }
    });
    $(".morelink").click(function () {
        if ($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
    $("input#input_text").focusin(function () {
        $(".typing-showup").addClass('is-active');
    });
    $(".cover-space .inspire-you input").focusin(function () {
        $(".drops-contain").addClass('is-active');
    });
});
$(document).on('click.bs.dropdown.data-api', '.dropdown.keep-inside-clicks-open', function (e) {
    e.stopPropagation();
});
$('.main-third-area span.pull-right i').click(function () {
    $(this).toggleClass('icon-heart2');
});

$("#password-field i").click(function () {
    if ($("#login_pass").attr("type") == "password") {
        $('#password-field i').addClass('icon-eye');
        $("#login_pass").attr("type", "text");
    }
    else {
        $('#password-field i').removeClass('icon-eye');
        $("#login_pass").attr("type", "password");
    }
});
$("#password-field1 i").click(function () {
    if ($("#login_pass1").attr("type") == "password") {
        $('#password-field1 i').addClass('icon-eye');
        $("#login_pass1").attr("type", "text");
    }
    else {
        $('#password-field1 i').removeClass('icon-eye');
        $("#login_pass1").attr("type", "password");
    }
});


