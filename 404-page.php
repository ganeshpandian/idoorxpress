<div class="error-head">
	<?php include 'header.php'; ?>
</div>

<div class="container-fluid nooo-page">
	<div class="page-not">
		<h1>Page Not Found</h1>
		<p>Seems like you lost your way.Lets bring you back home to <a href="#">Service Cambodia</a></p>
		<img src="dist/images/404-animate.gif">
	</div>
</div>

<div class="error-foot">
	<?php include 'footer.php'; ?>
</div>
