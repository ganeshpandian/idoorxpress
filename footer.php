<footer>
	<div class="footer-section container">	
		<ul>	
			<li><h3>Company</h3></li>
			<li><a href="#">Reviews</a></li>
			<li><a href="#">Coupons</a></li>
			<li><a href="#">Reviews</a></li>
			<li><a href="#">Coupons</a></li>
			<li><a href="#">Reviews</a></li>
			<li><a href="#">Coupons</a></li>
		</ul>
		<ul>
			<li><h3>SERVING IN</h3></li>
			<li><a href="#">Delhi NCR</a></li>
			<li><a href="#">Bangalore</a></li>
			<li><a href="#">Mumbai</a></li>
			<li><a href="#">Chennai</a></li>
			<li><a href="#">Reviews</a></li>

		</ul>
		<ul>	
			<li><h3>Company</h3></li>
			<li><a href="#">Reviews</a></li>
			<li><a href="#">Coupons</a></li>
			<li><a href="#">Reviews</a></li>
			<li><a href="#">Coupons</a></li>
		</ul>
		<ul>
			<li><h3>SERVING IN</h3></li>
			<li><a href="#">Delhi NCR</a></li>
			<li><a href="#">Bangalore</a></li>
			<li><a href="#">Mumbai</a></li>
			<li><a href="#">Chennai</a></li>
		</ul>
		<ul class="accept-cards">	
			<li><h3>We Accept</h3></li>
			<li class="visa-card"><a href="#"><img src="dist/images/visa-master.png"></a></li>
			<li><a href="#"><img src="dist/images/pipay.png"></a></li>
			<li class="aba-bank"><a href="#"><img src="dist/images/aba-bank.png"></a></li>
			<li><a href="#"><img src="dist/images/wing_new.png"></a></li>
			<li><h4>We Secure</h4></li>
			<li class="secure-pro"><a href="#"><img src="dist/images/saf.png"></a></li>
			<li class="secure-pro"><a href="#"><img src="dist/images/saf1.png"></a></li>
		</ul>
	</div>
</footer>
<div class="copyrights">
	<div class="container">	
		<div class="col-md-6 logo-years">
			<img src="dist/images/logo.png">© 2014-18 iDoorXpress Pvt. Ltd.
		</div>
		<div class="col-md-6 social-icons">
			<a href="#">Terms & Conditions</a>
			<a href="#">Policy</a>
			<ul>
				<li><a href="#"><i class="icon-facebook"></i></a></li>
				<li><a href="#"><i class="icon-twitter2"></i></a></li>
				<li><a href="#"><i class="icon-linkedin2"></i></a></li>
				<li><a href="#"><i class="icon-instagram2"></i></a></li>
				<li><a href="#"><i class="icon-youtube2"></i></a></li>
			</ul>
		</div>
	</div>
</div>
<script src="dist/scripts/main.js"></script>
</body>
</html>