<?php include 'header-loggedin.php'; ?>
<div class="container content-dash">
    <div class="tab-content">
      <div class="tab-pane active" id="vtab1">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#all" aria-expanded="true">All</a></li>
          <li><a data-toggle="tab" href="#awaiting_payment" aria-expanded="true">Awaiting Task</a></li>
          <li><a data-toggle="tab" href="#upcoming" aria-expanded="true">Upcoming</a></li>
          <li><a data-toggle="tab" href="#completed" aria-expanded="true">Completed</a></li>
          <li><a data-toggle="tab" href="#canceled" aria-expanded="true">Canceled</a></li>
        </ul>
        <div class="tab-content tasker-board">
            <div id="all" class="tab-pane fade in active">
              <table> 
                <thead> 
                    <tr>  
                        <th>Name</th>
                        <th>Reference Number</th>
                        <th>Task Type</th>
                        <th>Address</th>
                        <th>Booking Date</th>
                        <th>Task Status</th>
                    </tr>
                </thead>
                <tbody> 
                  <tr>  
                        <td>
                          <figure>
                            <img src="dist/images/review-img.jpg"></figure>
                            Aravinth Sakthivel <br>
                            <span>House Cleaning</span>    
                        </td>
                        <td>#R1597536842</td>
                        <td>Laptop Service</td>
                        <td>4th Floor, #216B,Preah Norodom Blvd Sangkat Tonle Bassac Khan Chamkarmorn Phnom Penh</td>
                        <td>10:00 AM</td>
                        <td class="status-btn await"><span>Awaiting</span></td>
                    </tr>
                    <tr>  
                        <td>
                          <figure>
                            <img src="dist/images/review-img.jpg"></figure>
                            Aravinth Sakthivel <br>
                            <span>House Cleaning</span>    
                        </td>
                        <td>#R1597536842</td>
                        <td>Laptop Service</td>
                        <td>4th Floor, #216B,Preah Norodom Blvd Sangkat Tonle Bassac Khan Chamkarmorn Phnom Penh</td>
                        <td>10:00 AM</td>
                        <td class="status-btn upcome"><span>Upcoming</span></td>
                    </tr>
                    <tr>  
                        <td>
                          <figure>
                            <img src="dist/images/review-img.jpg"></figure>
                            Aravinth Sakthivel <br>
                            <span>House Cleaning</span>    
                        </td>
                        <td>#R1597536842</td>
                        <td>Laptop Service</td>
                        <td>4th Floor, #216B,Preah Norodom Blvd Sangkat Tonle Bassac Khan Chamkarmorn Phnom Penh</td>
                        <td>10:00 AM</td>
                        <td class="status-btn comple"><span>Completed</span></td>
                    </tr>
                    <tr>  
                        <td>
                          <figure>
                            <img src="dist/images/review-img.jpg"></figure>
                            Aravinth Sakthivel <br>
                            <span>House Cleaning</span>    
                        </td>
                        <td>#R1597536842</td>
                        <td>Laptop Service</td>
                        <td>4th Floor, #216B,Preah Norodom Blvd Sangkat Tonle Bassac Khan Chamkarmorn Phnom Penh</td>
                        <td>10:00 AM</td>
                        <td class="status-btn cancel"><span>Cancel</span></td>
                    </tr>
                  <?php for($i = 0; $i < 15; $i ++): ?>
                    <tr>  
                        <td>
                          <figure>
                            <img src="dist/images/review-img.jpg"></figure>
                            Aravinth Sakthivel <br>
                            <span>House Cleaning</span>    
                        </td>
                        <td>#R1597536842</td>
                        <td>Laptop Service</td>
                        <td>4th Floor, #216B,Preah Norodom Blvd Sangkat Tonle Bassac Khan Chamkarmorn Phnom Penh</td>
                        <td>10:00 AM</td>
                        <td class="status-btn await"><span>Awaiting</span></td>
                    </tr>
                    <?php endfor; ?>
                </tbody>
              </table>
            </div>
            <div id="awaiting_payment" class="tab-pane fade">
              <table> 
                <thead> 
                    <tr>  
                        <th>Name</th>
                        <th>Reference Number</th>
                        <th>Task Type</th>
                        <th>Address</th>
                        <th>Booking Date</th>
                        <th>Task Status</th>
                    </tr>
                </thead>
                <tbody> 
                  <tr>  
                        <td>
                          <figure>
                            <img src="dist/images/review-img.jpg"></figure>
                            Aravinth Sakthivel <br>
                            <span>House Cleaning</span>    
                        </td>
                        <td>#R1597536842</td>
                        <td>Laptop Service</td>
                        <td>4th Floor, #216B,Preah Norodom Blvd Sangkat Tonle Bassac Khan Chamkarmorn Phnom Penh</td>
                        <td>10:00 AM</td>
                        <td class="status-btn await"><span>Awaiting</span></td>
                    </tr>
                  </tbody>
              </table>
            </div>
            <div id="upcoming" class="tab-pane fade">
              <table> 
                <thead> 
                    <tr>  
                        <th>Name</th>
                        <th>Reference Number</th>
                        <th>Task Type</th>
                        <th>Address</th>
                        <th>Booking Date</th>
                        <th>Task Status</th>
                    </tr>
                </thead>
                <tbody> 
                  <tr>  
                        <td>
                          <figure>
                            <img src="dist/images/review-img.jpg"></figure>
                            Aravinth Sakthivel <br>
                            <span>House Cleaning</span>    
                        </td>
                        <td>#R1597536842</td>
                        <td>Laptop Service</td>
                        <td>4th Floor, #216B,Preah Norodom Blvd Sangkat Tonle Bassac Khan Chamkarmorn Phnom Penh</td>
                        <td>10:00 AM</td>
                        <td class="status-btn upcome"><span>Upcoming</span></td>
                    </tr>
                  </tbody>
              </table>
            </div>
            <div id="completed" class="tab-pane fade">
              <table> 
                <thead> 
                    <tr>  
                        <th>Name</th>
                        <th>Reference Number</th>
                        <th>Task Type</th>
                        <th>Address</th>
                        <th>Booking Date</th>
                        <th>Task Status</th>
                    </tr>
                </thead>
                <tbody> 
                  <tr>  
                        <td>
                          <figure>
                            <img src="dist/images/review-img.jpg"></figure>
                            Aravinth Sakthivel <br>
                            <span>House Cleaning</span>    
                        </td>
                        <td>#R1597536842</td>
                        <td>Laptop Service</td>
                        <td>4th Floor, #216B,Preah Norodom Blvd Sangkat Tonle Bassac Khan Chamkarmorn Phnom Penh</td>
                        <td>10:00 AM</td>
                        <td class="status-btn comple"><span>Completed</span></td>
                    </tr>
                  </tbody>
              </table>
            </div>
            <div id="canceled" class="tab-pane fade">
             <table> 
                <thead> 
                    <tr>  
                        <th>Name</th>
                        <th>Reference Number</th>
                        <th>Task Type</th>
                        <th>Address</th>
                        <th>Booking Date</th>
                        <th>Task Status</th>
                    </tr>
                </thead>
                <tbody> 
                  <tr>  
                        <td>
                          <figure>
                            <img src="dist/images/review-img.jpg"></figure>
                            Aravinth Sakthivel <br>
                            <span>House Cleaning</span>    
                        </td>
                        <td>#R1597536842</td>
                        <td>Laptop Service</td>
                        <td>4th Floor, #216B,Preah Norodom Blvd Sangkat Tonle Bassac Khan Chamkarmorn Phnom Penh</td>
                        <td>10:00 AM</td>
                        <td class="status-btn cancel"><span>Cancel</span></td>
                    </tr>
                  </tbody>
              </table>
            </div>
          </div>
      </div>
    </div>
  </div>

<?php include 'footer.php'; ?>