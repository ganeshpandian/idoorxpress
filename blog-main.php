<?php include 'header-loggedin.php'; ?>
<div class="container-fluid video-area p-0">
	<iframe width="100%" height="200" src="https://www.youtube.com/embed/R6xFZcEx7pg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<div class="container-fluid cover-space">
	<div class="container inspire-you">
		<h3>Find your inspiration</h3>
		<label>
			<i class="icon-search"></i>
			<input type="text" name="" placeholder="Discover a Place">
			<div class="drops-contain">
				<ul>
					<?php for ($i = 0; $i < 10; $i++) : ?>
						<li>
							<a href="#">Find your inspiration on the blog page</a>
						</li>
					<?php endfor; ?>
				</ul>
			</div>
		</label>
	</div>
</div>
<div class="container blog-moves">
	<div class="container-fluid owl-carousel explore_blog_slider" id="explore_blog_slider">
		<?php for ($i = 0; $i < 10; $i++) : ?>
			<div class="item">
				<figure><img src="dist/images/blog-5.jpg" /></figure>
				<div class="narrow-content-box">
					<a href="#">Thailand</a>
				</div>
			</div>
			<div class="item">
				<figure><img src="dist/images/blog-4.jpg" /></figure>
				<div class="narrow-content-box">
					<a href="#">Malasiya</a>
				</div>
			</div>
			<div class="item">
				<figure><img src="dist/images/blog-3.jpg" /></figure>
				<div class="narrow-content-box">
					<a href="#">Singapore</a>
				</div>
			</div>
			<div class="item">
				<figure><img src="dist/images/blog-2.jpg" /></figure>
				<div class="narrow-content-box">
					<a href="#">Australia</a>
				</div>
			</div>
		<?php endfor; ?>
	</div>
</div>
<div class="blog-page container">
	<div class="main-blog-area">
		<ul>
			<li>
				<figure><img src="dist/images/blog-1.jpg"></figure>
				<div class="text-depend">
					<a href="#">
						<b>Thailand</b>
						<h3>These Spirit Houses In Thailand May Save Your Life</h3>
					</a>
				</div>
			</li>
			<li>
				<figure>
					<iframe width="100%" height="200" src="https://www.youtube.com/embed/R6xFZcEx7pg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</figure>
				<div class="text-depend">
					<a href="#">
						<b>Thailand</b>
						<h3>These Spirit Houses In Thailand May Save Your Life</h3>
					</a>
				</div>
			</li>
		</ul>
	</div>
	<div class="main-third-area">
		<ul>
			<?php for ($i = 0; $i < 5; $i++) : ?>
				<li>
					<figure class="video-seg"><img src="dist/images/blog-3.jpg"><i class="icon-controller-play"></i></figure>
					<span class="pull-right"><i class="icon-heart-outlined"></i></span>
					<div class="text-head">
						<a href="#">
							<b>Thailand</b>
							<h3>These Spirit Houses In Thailand May Save Your Life</h3>
						</a>
					</div>
				</li>
				<li>
					<figure><img src="dist/images/blog-4.jpg"></figure>
					<span class="pull-right"><i class="icon-heart-outlined"></i></span>
					<div class="text-head">
						<a href="#">
							<b>Thailand</b>
							<h3>These Spirit Houses In Thailand May Save Your Life</h3>
						</a>
					</div>
				</li>
				<li>
					<figure><img src="dist/images/blog-5.jpg"></figure>
					<span class="pull-right"><i class="icon-heart-outlined"></i></span>
					<div class="text-head">
						<a href="#">
							<b>Thailand</b>
							<h3>These Spirit Houses In Thailand May Save Your Life</h3>
						</a>
					</div>
				</li>
			<?php endfor; ?>
		</ul>
	</div>
</div>
<?php include 'footer.php'; ?>