<?php include 'header.php'; ?>
<div class="container-fluid book-area">
    <div class="container book-dec">
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 pl-0">
            <div class="send-pack">
                <h3 class="head-hdd">Send packages</h3>
                <p>Your on demand local courier</p>
                <div class="pick-up active" data-toggle="modal" data-target="#pick_modal">
                    <span>PICK UP ADDRESS <b>*</b></span>
                    <input type="text" name="" id="" placeholder="Search pickup location" disabled>
                    <i class="icon-search"></i>
                </div>
                <div class="pick-up">
                    <span>DELIVERY ADDRESS <b>*</b></span>
                    <input type="text" name="" id="" placeholder="Search drop location" disabled>
                    <i class="icon-search"></i>
                </div>
                <div class="form-area">
                    <label>
                        <input type="text" name="" id="" placeholder="Any Instructions? e.g., 'Leave delivery with security'">
                        <i class="icon-menu"></i>
                    </label>
                </div>
                <div class="by-confirm">
                    <p>By confirming I accept this order doesn’t contain illegal/resticted items, if illegal/restricted items are found by Dunzo Partner, they may report it to the law enforcement authorities. <a href="#">Terms and Conditions</a> </p>
                </div>
                <a href="#" class="conf-btn theme_btn">Confirm Order</a>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="pick_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <h3>Search Pickup Location</h3>
                        <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                            <i class="icon-cross"></i>
                        </button>
                        <div class="enter-addr">
                            <input type="text" name="" id="" placeholder="Address search e.g., Nilgiri's HSR">
                            <i class="icon-search"></i>
                        </div>
                        <div class="map-geo">
                            <a href="#"><i class="icon-map"></i>Select location via map</a>
                        </div>
                        <br>
                        <br>
                        <div class="no-data">
                            <img src="dist/images/bike.png" alt="">
                        </div>
                        <div class="not-found">
                            <img src="dist/images/bike.png" alt="">
                            <h3>Non Serviceable Area!</h3>
                            <p>Sevadoek services are not available at this location yet.
                                Please update your selected location.</p>
                        </div>
                        <div class="user-details">
                            <div class="form-area">
                                <label>Flat, Floor, Building Name <b>*</b></label>
                                <input type="text" name="" id="" placeholder="E.g. 2nd floor, Sevadoek office">
                                <i class="icon-home"></i>
                            </div>
                            <div class="form-area">
                                <label>Flat, Floor, Building Name <b>*</b></label>
                                <input type="text" name="" id="" placeholder="E.g. 2nd floor, Sevadoek office">
                                <i class="icon-map"></i>
                            </div>
                            <div class="form-area">
                                <label>Flat, Floor, Building Name <b>*</b></label>
                                <input type="text" name="" id="" placeholder="E.g. 2nd floor, Sevadoek office">
                                <i class="icon-phone"></i>
                            </div>
                            <div class="form-area">
                                <label>Flat, Floor, Building Name <b>*</b></label>
                                <input type="text" name="" id="" placeholder="E.g. 2nd floor, Sevadoek office">
                                <i class="icon-user"></i>
                            </div>
                            <div class="form-area">
                                <label for="weight">Select Weight</label>
                                <select id="weight">
                                    <option value="5">Upto 5 Kg</option>
                                    <option value="10">Upto 10 Kg</option>
                                    <option value="20">Upto 15 Kg</option>
                                    <option value="15">Upto 20 Kg</option>
                                </select>
                            </div>
                            <div class="form-area">
                                <label>Who wil Pay</label>
                                <div class="each-radio">
                                    <label><input type="radio" name="optradio" checked>Sender</label>
                                </div>
                                <div class="each-radio">
                                    <label><input type="radio" name="optradio" checked>Receiver</label>
                                </div>
                            </div>
                            <div class="form-area">
                                <div class="custom-file">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                    <input type="file" class="custom-file-input" id="customFile">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 p-0">
            <div class="some-dos inv-pdf">
                <h3 class="head-hdd">Invoice</h3>
                <ul>
                    <li>Sevacam cash cannot be used to send Packages</li>
                    <li>Delivery Charges may  changes  after signing in</li>
                </ul>
                <div class="serv-fee">
                    <b>Partner delivery fee <i class="icon-alert-circle"></i></b>
                    <span>$ 90</span>
                </div>
                <hr>
                <div class="serv-fee">
                    <b>To Pay</b>
                    <strong>$90</strong>
                </div>
            </div>
            <div class="some-dos">
                <h3 class="head-hdd">Some do’s & Dont’s</h3>
                <div class="no-pur">
                    <p>
                        <b>No Purchases -</b>
                        Delivery partner would not be able to make any purchase
                    </p>
                </div>
                <div class="no-pur">
                    <p>
                        <b>No Autos/Cabs -</b>
                        We will not be able to get something transported via these
                    </p>
                </div>
                <div class="no-pur">
                    <p>
                        <b>Package Weight -</b>
                        We cannot deliver items that can’t be easily carried on bike
                    </p>
                </div>
                <div class="no-pur">
                    <p>
                        <b>Restricted/Illegal Item -</b>
                        Please don’t hand over any restricted item
                    </p>
                </div>
                <img src="dist/images/bike.png" alt="">
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>