<?php include 'header-loggedin.php'; ?>
	<div class="container-fluid inte-deisgn">
		<div class="page-comer container">
			Home / pagelisting
		</div>
		<ul class="container">
			<h2>Interior Designers Near Me</h2>
			<div class="container-fluid owl-carousel">
				<?php for($i = 0; $i < 5; $i ++): ?>
				<div class="item">
					<li data-toggle="modal" data-target="#booknow_first">
						<span>$20</span>
						<figure><img src="dist/images/interior1.jpg"></figure>
						<h3>Choose and book from 100+ services and track them on the go with the iDoorXpress App</h3>
						<a href="#" class="button-green">Get Free Quotes</a>
					</li>
				</div>
				<div class="item">
					<li data-toggle="modal" data-target="#booknow_first">
						<span>$20</span>
						<figure><img src="dist/images/interior2.jpg"></figure>
						<h3>Choose and book from 100+ services and track them on the go with the iDoorXpress App</h3>
						<a href="#" class="button-green">Get Free Quotes</a>
					</li>
				</div>
				<div class="item">
					<li data-toggle="modal" data-target="#booknow_first">
						<span>$20</span>
						<figure><img src="dist/images/interior1.jpg"></figure>
						<h3>Choose and book from 100+ services and track them on the go with the iDoorXpress App</h3>
						<a href="#" class="button-green">Get Free Quotes</a>
					</li>
				</div>
				<div class="item">
					<li data-toggle="modal" data-target="#booknow_first">
						<span>$20</span>
						<figure><img src="dist/images/interior2.jpg"></figure>
						<h3>Choose and book from 100+ services and track them on the go with the iDoorXpress App</h3>
						<a href="#" class="button-green">Get Free Quotes</a>
					</li>
				</div>
				<?php endfor; ?> 
			</div>
		</ul>
	</div>
	<div class="container-fluid listing-menu">
		<div class="container list-navi">
			<div class="navigate-menu" data-spy="affix" data-offset-top="700">
				<nav class="navigation container" id="mainNav">
					<a class="navigation__link active" href="#why_idoor">Why idoorxpress?</a>
					<a class="navigation__link" href="#how_works">How it Works</a>
					<a class="navigation__link" href="#customer_review">Customer reviews</a>
					<a class="navigation__link" href="#express_faqs">FAQ's</a>
					<a class="navigation__link" href="#hire_guide">Hiring Guide</a>
				</nav>
			</div>
			<div class="container" id="both_area">
				<div class="content-views">
					<div class="page-section" id="why_idoor">
						<h2>Why idoorXpress</h2>
						<div class="proj-exec">
							<h3 data-toggle="collapse" data-target="#demo"><img src="dist/images/fav.png"> Small to large project execution</h3>
							<div id="demo" class="collapse in">
								<ul>
									<li>Pellentesque dolor eros, placerat sit amet consequat in, iaculis eget elit.</li>
									<li>Nullam consectetur ipsum sit amet magna ultrices, eget ornare mi ultrices.</li>
								</ul>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis lorem placerat, pretium purus eget, efficitur mauris. Integer ut urna sagittis, maximus orci vitae, vestibulum mi. Nulla bibendum odio a ipsum accumsan fermentum. Suspendisse ac ultrices lectus, posuere accumsan diam. </p>
							</div>
						</div>
						<div class="proj-exec">
							<h3 data-toggle="collapse" data-target="#demo1"><img src="dist/images/fav.png"> Small to large project execution</h3>
							<div id="demo1" class="collapse">
								<ul>
									<li>Pellentesque dolor eros, placerat sit amet consequat in, iaculis eget elit.</li>
									<li>Nullam consectetur ipsum sit amet magna ultrices, eget ornare mi ultrices.</li>
								</ul>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis lorem placerat, pretium purus eget, efficitur mauris. Integer ut urna sagittis, maximus orci vitae, vestibulum mi. Nulla bibendum odio a ipsum accumsan fermentum. Suspendisse ac ultrices lectus, posuere accumsan diam. </p>
							</div>
						</div>
						<div class="proj-exec">
							<h3 data-toggle="collapse" data-target="#demo2"><img src="dist/images/fav.png"> Small to large project execution</h3>
							<div id="demo2" class="collapse">
								<ul>
									<li>Pellentesque dolor eros, placerat sit amet consequat in, iaculis eget elit.</li>
									<li>Nullam consectetur ipsum sit amet magna ultrices, eget ornare mi ultrices.</li>
								</ul>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis lorem placerat, pretium purus eget, efficitur mauris. Integer ut urna sagittis, maximus orci vitae, vestibulum mi. Nulla bibendum odio a ipsum accumsan fermentum. Suspendisse ac ultrices lectus, posuere accumsan diam. </p>
							</div>
						</div>
					</div>
					<div class="page-section" id="how_works">
						<h2>How it Works</h2>
						<div class="work-space">
							<?php for($i = 0; $i < 5; $i ++): ?>
							<ul>
								<li><img src="dist/images/fav.png"></li>
								<li>
									<h4>Share your requirements</h4>
									<p>Type of Project, Area of Property etc.</p>
								</li>
							</ul>
							<?php endfor; ?> 
						</div> 
					</div>
					<div class="page-section" id="customer_review">
						<h2>Customer Reviews</h2>
						<div class="rate-star">
							<h3>Top Recent Customer Reviews</h3>
							<p>of Party Make-up Artists in Bangalore</p>
							<h4><span><i class="icon-star2"></i>4.5</span>based on 1262 ratings</h4>
							<hr>
							<?php for($i = 0; $i < 6; $i ++): ?>
							<div class="rating-area">
								<figure><img src="dist/images/review-img.jpg"></figure>
								<h4>Shruthi Santosh <small>hired deepa c</small></h4>
								<h5><span><i class="icon-star2"></i>4.5</span>on 17 November, 2018</h5>
								<p class="mores">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu vestibulum turpis, eu euismod purus. Nam pharetra risus in orci efficitur bibendum. Nam vitae facilisis ex. Cras vel porttitor orci. Vestibulum metus massa, efficitur a laoreet quis, blandit non lacus.</p>
							</div>
							<?php endfor; ?> 
						</div>
					</div>
					<div class="page-section" id="express_faqs">
						<h2>FAQ's</h2>
						<div class="proj-exec">
							<h3 data-toggle="collapse" data-target="#demo3"><img src="dist/images/fav.png"> Small to large project execution</h3>
							<div id="demo3" class="collapse in">
								<ul>
									<li>Pellentesque dolor eros, placerat sit amet consequat in, iaculis eget elit.</li>
									<li>Nullam consectetur ipsum sit amet magna ultrices, eget ornare mi ultrices.</li>
								</ul>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis lorem placerat, pretium purus eget, efficitur mauris. Integer ut urna sagittis, maximus orci vitae, vestibulum mi. Nulla bibendum odio a ipsum accumsan fermentum. Suspendisse ac ultrices lectus, posuere accumsan diam. </p>
							</div>
						</div>
						<div class="proj-exec">
							<h3 data-toggle="collapse" data-target="#demo4"><img src="dist/images/fav.png"> Small to large project execution</h3>
							<div id="demo4" class="collapse">
								<ul>
									<li>Pellentesque dolor eros, placerat sit amet consequat in, iaculis eget elit.</li>
									<li>Nullam consectetur ipsum sit amet magna ultrices, eget ornare mi ultrices.</li>
								</ul>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis lorem placerat, pretium purus eget, efficitur mauris. Integer ut urna sagittis, maximus orci vitae, vestibulum mi. Nulla bibendum odio a ipsum accumsan fermentum. Suspendisse ac ultrices lectus, posuere accumsan diam. </p>
							</div>
						</div>
						<div class="proj-exec">
							<h3 data-toggle="collapse" data-target="#demo5"><img src="dist/images/fav.png"> Small to large project execution</h3>
							<div id="demo5" class="collapse">
								<ul>
									<li>Pellentesque dolor eros, placerat sit amet consequat in, iaculis eget elit.</li>
									<li>Nullam consectetur ipsum sit amet magna ultrices, eget ornare mi ultrices.</li>
								</ul>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis lorem placerat, pretium purus eget, efficitur mauris. Integer ut urna sagittis, maximus orci vitae, vestibulum mi. Nulla bibendum odio a ipsum accumsan fermentum. Suspendisse ac ultrices lectus, posuere accumsan diam. </p>
							</div>
						</div>
					</div>
					<div class="page-section" id="hire_guide">
						<h2>Hiring Guide</h2>
						<div class="proc-guide">
							<h4>Integer cursus dolor non cursus cursus</h4>
							<p>Ut magna lorem, egestas vel dolor et, faucibus placerat tortor. Aliquam imperdiet orci sit amet enim rhoncus auctor. Phasellus ex nibh, pulvinar at posuere et, aliquam eget purus. Aenean rhoncus aliquet dui ac sodales. Nam id placerat lectus, eu fermentum sem. </p>
							<ul>
								<?php for($i = 0; $i < 9; $i ++): ?>
								<li><b>Curabitur egestas purus ut ex facilisis viverra.</b> Donec vehicula leo nec risus blandit, id vulputate dui eleifend. Donec blandit odio sed vulputate tempus.Ut at sapien interdum, ultrices mauris sit amet, finibus risus. Nullam lectus tortor, porta sed diam cursus, accumsan imperdiet massa. Maecenas finibus pulvinar vulputate. Aliquam vehicula eget erat sit amet faucibus. </li>
								<?php endfor; ?> 
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php include 'footer.php'; ?>

<div class="modal fade" id="booknow_first" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<i class="icon-circle-with-cross"></i>
		</button>
      <div class="proj-views-type">
      	<h3>Cleaning Types</h3>
      	<ul>
      		<li><i class="icon-check2"></i>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
      		<li><i class="icon-check2"></i>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
      		<li><i class="icon-check2"></i>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
      		<li><i class="icon-check2"></i>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
      		<li><i class="icon-check2"></i>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
      		<li><i class="icon-check2"></i>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
      		<li><i class="icon-check2"></i>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
      	</ul>
      	<a data-toggle="modal" data-dismiss="modal" data-target="#booknow_pops">Continue</a>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="booknow_pops" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="address-locate">
      	<h2>Confirm Booking</h2>
  		<h3>Date</h3>
  		<div class="container-fluid owl-carousel date-select">
  			<div class="item">
  				<div class="actives">
  					<span>14</span><b>Friday</b>
  				</div>
  			</div>
  			<div class="item">
  				<div class="">
  					<span>15</span><b>Saturday</b>
  				</div>
  			</div>
  			<div class="item">
  				<div class="">
  					<span>16</span><b>Sunday</b>
  				</div>
  			</div>
  			<div class="item">
  				<div class="">
  					<span>17</span><b>Monday</b>
  				</div>
  			</div>
  			<div class="item">
  				<div class="">
  					<span>18</span><b>Tuesday</b>
  				</div>
  			</div>
  			<div class="item">
  				<div class="">
  					<span>19</span><b>Wednesday</b>
  				</div>
  			</div>
  			<div class="item">
  				<div class="">
  					<span>20</span><b>Thursday</b>
  				</div>
  			</div>
  		</div>
      </div>
      <div class="select-location">
      	<h3>Select Location</h3>
      	<button><i class="icon-map-pin"></i> Add Current Location</button>
      </div>
      <div class="map-selectoptions">
      	<div class="col-md-7">
	      	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3994260.874372215!2d102.73614034322199!3d12.13639810916273!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x310787bfd4dc3743%3A0xe4b7bfe089f41253!2sCambodia!5e0!3m2!1sen!2skh!4v1544775209910" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
	      </div>
	      <div class="col-md-5 select-locations">
	      	<h3>Description</h3>
	      	<textarea class="form-control" rows="5" id="comment"></textarea>
	      	<input type="submit" name="" value="book now">
	      </div>
      </div>
    </div>
  </div>
</div>
