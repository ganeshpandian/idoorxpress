<?php include 'header.php'; ?>
<div class="container-fluid contact-banner">
	<img src="dist/images/contact-us.jpg">
</div>
<div class="container conatct-mail">
	<div class="col-md-6 company-cnt">
		<h3>Contact Us</h3>
		<div class="check-add">
			<h4>Address</h4>
			<p>No. 464A Monivong Blvd,Sangkat Tonle Bassac,Khan Chamkarmorn, Phnom Penh Cambodia</p>
		</div>
		<div class="check-add">
			<h4>Email</h4>
			<span>Customer services: <a href="#">sevacam.services@gmail.com</a></span>
			<span>Other requests: <a href="#">sevacam@gmail.com</a></span>
		</div>
		<div class="check-add">
			<h4>Give us a call</h4>
			<p>For expert information about Sevacam services, please call our 24/7 Call Center on <a href="#">+855  70 970 851</a></p>
		</div>
	</div>
	<div class="col-md-6 company-mail">
		<h3>Fill the form</h3>
		<form>
			<div class="form-group">
				<label>Name</label>
			    <input type="text" id="fst_name" class="form-control" required="">
			</div>
			<div class="form-group">
				<label>Email</label>
			    <input type="text" id="fst_name" class="form-control" required="">
			</div>
			<div class="form-group">
				<label>Phone Number</label>
			    <input type="number" id="fst_name" class="form-control" required="">
			</div>
			<div class="form-group">
				<label>Subject</label>
			    <input type="text" id="fst_name" class="form-control" required="">
			</div>
			<div class="form-group">
				<label>Message</label>
			    <textarea></textarea>
			</div>
			<input type="submit" name="" value="submit">
		</form>
	</div>
</div>
<div class="container map-view">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d250151.45210671265!2d104.75010167580578!3d11.57933057539524!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3109513dc76a6be3%3A0x9c010ee85ab525bb!2sPhnom+Penh%2C+Cambodia!5e0!3m2!1sen!2sus!4v1558165115970!5m2!1sen!2sus" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>

<?php include 'footer.php'; ?>