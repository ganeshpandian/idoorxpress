<?php include 'header.php'; ?>


<div class="banner-area">
	<div class="banner-content">
		<h1>Your Service Expert</h1>
		<h2>Get instant access to reliable and affordable services</h2>
		<div class="select-area container">
			<div class="dropdown select-place">
			  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			    <i class="icon-send"></i> Phnom penh <i class="icon-chevron-down"></i> 
			  </button>
			  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
			    <a class="dropdown-item" href="#">Battabang</a>
			    <a class="dropdown-item" href="#">Siemriep</a>
			    <a class="dropdown-item" href="#">Ankoor watt</a>
			    <a class="dropdown-item" href="#">Battabang</a>
			    <a class="dropdown-item" href="#">Siemriep</a>
			    <a class="dropdown-item" href="#">Ankoor watt</a>
			    <a class="dropdown-item" href="#">Battabang</a>
			    <a class="dropdown-item" href="#">Siemriep</a>
			    <a class="dropdown-item" href="#">Ankoor watt</a>
			  </div>
			</div>
			<div class="enter-area">
				<input type="text" class="form-control" id="input_text" aria-describedby="inputtype" placeholder="Search for a service">
				<b>E.g. Salon at Home, Plumber, Wedding Photographer</b>
				<div class="typing-showup">
					<div class="services-search">
						<a href="#"><img src="dist/images/fav.png">Appliance Repair</a>
						<a href="#"><img src="dist/images/fav.png">Home Cleaning</a>
						<a href="#"><img src="dist/images/fav.png">Appliance Repair</a>
						<a href="#"><img src="dist/images/fav.png">Home Cleaning</a>
						<a href="#"><img src="dist/images/fav.png">Appliance Repair</a>
						<a href="#"><img src="dist/images/fav.png">Home Cleaning</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="service-provided">
	<ul class="repair-services">
		<li data-toggle="modal" data-target="#show_services"><img src="dist/images/svg/air.svg" class="svg"><h6>Appliance repair</h6></li>
		<li><img src="dist/images/svg/heart.svg" class="svg"><h6>Beauty & Spa</h6></li>
		<li><img src="dist/images/svg/roll.svg" class="svg"><h6>Home cleaning & repairs</h6></li>
		<li><img src="dist/images/svg/suitcase.svg" class="svg"><h6>Business & taxes</h6></li>
		<li><img src="dist/images/svg/automobile.svg" class="svg"><h6>personal & more</h6></li>
		<li><img src="dist/images/svg/rings.svg" class="svg"><h6>Wedding & events</h6></li>
	</ul>
</div>
<div class="container listof-services">
	<h3>Recommended Services</h3>
	<div class="container-fluid owl-carousel">
		<?php for($i = 0; $i <1; $i ++): ?>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair-1.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair-2.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair-3.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <?php endfor; ?> 
	</div>
</div>
<div class="container listof-services">
	<h3>Appliance Repair</h3>
	<div class="container-fluid owl-carousel">
		<?php for($i = 0; $i <1; $i ++): ?>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair-1.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair-2.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair-3.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <?php endfor; ?> 
	</div>
</div>
<div class="container listof-services">
	<h3>Home Cleaning</h3>
	<div class="container-fluid owl-carousel">
		<?php for($i = 0; $i <1; $i ++): ?>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair-1.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair-2.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair-3.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <?php endfor; ?> 
	</div>
</div>
<div class="container listof-services">
	<h3>Shifting Homes</h3>
	<div class="container-fluid owl-carousel">
		<?php for($i = 0; $i <1; $i ++): ?>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair-1.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair-2.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair-3.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <?php endfor; ?> 
	</div>
</div>
<div class="container listof-services">
	<h3>Painting & Renovation</h3>
	<div class="container-fluid owl-carousel">
		<?php for($i = 0; $i <1; $i ++): ?>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair-1.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair-2.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair-3.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <?php endfor; ?> 
	</div>
</div>
<div class="container listof-services">
	<h3>Wedding Services</h3>
	<div class="container-fluid owl-carousel">
		<?php for($i = 0; $i <1; $i ++): ?>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair-1.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair-2.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <div class="item">
	        <a href="#"><figure><img src="dist/images/repair-3.jpg"/></figure></a>
	        <div class="narrow-content-box">
	        	<h5>Get 30% on Service deals</h5>
	        	<a href="#">View More <i class="icon-chevron-right"></i></a>
	        </div>
	    </div>
	    <?php endfor; ?> 
	</div>
</div>
<div class="download-app">
	<div class="container">
		<div class="col-md-5">
			<figure><img src="dist/images/application.png"></figure>
		</div>	
		<div class="col-md-7 google-ios">
			<h3>Download the App</h3>
			<p>Choose and book from 100+ services and track them on the go with the iDoorXpress App</p>
			<ul>
				<li><img src="dist/images/appStore.png"></li>
				<li><img src="dist/images/googlePlay.png"></li>
			</ul>
		</div>
	</div>
</div>		
<div class="container review-section">
	<h2>Customer Reviews</h2>
	<ul>
		<?php for($i = 0; $i < 4; $i ++): ?>
		<li>
			<div class="review-name">
				<figure><img src="dist/images/review-img.jpg"></figure>
				<h3>Soniya Deeksha</h3>
				<h4>San Francisco, CA</h4>
			</div>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer in fringilla ipsum. Morbi libero arcu, feugiat vel justo et, ullamcorper posuere ante. Ut nec rhoncus felis.</p>
		</li>
		<?php endfor; ?>
		<li>
			<div class="review-name">
				<figure><img src="dist/images/review-img.jpg"></figure>
				<h3>Soniya Deeksha</h3>
				<h4>San Francisco, CA</h4>
			</div>
			<p>Good</p>
		</li> 
		<li>
			<div class="review-name">
				<figure><img src="dist/images/review-img.jpg"></figure>
				<h3>Soniya Deeksha</h3>
				<h4>San Francisco, CA</h4>
			</div>
			<p>Good Worflow with Service Cambodia</p>
		</li> 
		<a href="#" class="text-center">View All Reviews</a>
	</ul>	
</div>

<?php include 'footer.php'; ?>


<div class="modal" id="show_services" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
 	<div class="services-list">
 		<a href="#" class="desktop-hide"><i class="icon-cross"></i></a>
 		<div class="col-md-5 name-service">
		    <div class="nav vab-label" id="v-pills-tab" role="tablist" aria-orientation="vertical">
		    	<a class="nav-link active" data-toggle="pill" href="#app_repair" role="tab" aria-selected="true">
		    		<img src="dist/images/svg/air.svg" class="svg">
		    		<h6>Appliance repair</h6>
		    	</a>
		    	<a class="nav-link" data-toggle="pill" href="#beauty_spa" role="tab" aria-selected="false">
		    		<img src="dist/images/svg/heart.svg" class="svg">
		    		<h6>Beauty & Spa</h6>
		    	</a>
		    	<a class="nav-link" data-toggle="pill" href="#hme_clean" role="tab" aria-selected="false">
		    		<img src="dist/images/svg/roll.svg" class="svg">
		    		<h6>Home cleaning & repairs</h6>
		    	</a>
		    	<a class="nav-link" data-toggle="pill" href="#business_tax" role="tab" aria-selected="false">
		    		<img src="dist/images/svg/suitcase.svg" class="svg">
		    		<h6>Business & taxes</h6>
		    	</a>
		    	<a class="nav-link" data-toggle="pill" href="#person_more" role="tab" aria-selected="false">
		    		<img src="dist/images/svg/automobile.svg" class="svg">
		    		<h6>personal & more</h6>
		    	</a>
		    	<a class="nav-link" data-toggle="pill" href="#wedding_event" role="tab" aria-selected="false">
		    		<img src="dist/images/svg/rings.svg" class="svg">
		    		<h6>Wedding & events</h6>
		    	</a>
		    </div>
		  </div>
		  <div class="col-md-7 select-service">
		    <div class="tab-content" id="v-pills-tabContent">
				<div class="tab-pane fade active in" id="app_repair" role="tabpanel">
					<div class="list-of-services">
						<a href="#" class="service-provide"><img src="dist/images/svg/air.svg" class="svg"><h6>Appliance repair</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/heart.svg" class="svg"><h6>Beauty & Spa</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/roll.svg" class="svg"><h6>Home cleaning & repairs</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/suitcase.svg" class="svg"><h6>Business & taxes</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/automobile.svg" class="svg"><h6>personal & more</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/rings.svg" class="svg"><h6>Wedding & events</h6></a>
					</div>
				</div>
				<div class="tab-pane fade" id="beauty_spa" role="tabpanel">
					<div class="list-of-services">
						<a href="#" class="service-provide"><img src="dist/images/svg/air.svg" class="svg"><h6>Appliance repair</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/heart.svg" class="svg"><h6>Beauty & Spa</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/roll.svg" class="svg"><h6>Home cleaning & repairs</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/suitcase.svg" class="svg"><h6>Business & taxes</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/automobile.svg" class="svg"><h6>personal & more</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/rings.svg" class="svg"><h6>Wedding & events</h6></a>
					</div>
				</div>
				<div class="tab-pane fade" id="hme_clean" role="tabpanel">
					<div class="list-of-services">
						<a href="#" class="service-provide"><img src="dist/images/svg/air.svg" class="svg"><h6>Appliance repair</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/heart.svg" class="svg"><h6>Beauty & Spa</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/roll.svg" class="svg"><h6>Home cleaning & repairs</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/suitcase.svg" class="svg"><h6>Business & taxes</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/automobile.svg" class="svg"><h6>personal & more</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/rings.svg" class="svg"><h6>Wedding & events</h6></a>
					</div>
				</div>
				<div class="tab-pane fade" id="business_tax" role="tabpanel">
					<div class="list-of-services">
						<a href="#" class="service-provide"><img src="dist/images/svg/air.svg" class="svg"><h6>Appliance repair</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/heart.svg" class="svg"><h6>Beauty & Spa</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/roll.svg" class="svg"><h6>Home cleaning & repairs</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/suitcase.svg" class="svg"><h6>Business & taxes</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/automobile.svg" class="svg"><h6>personal & more</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/rings.svg" class="svg"><h6>Wedding & events</h6></a>
					</div>
				</div>
				<div class="tab-pane fade" id="person_more" role="tabpanel">
					<div class="list-of-services">
						<a href="#" class="service-provide"><img src="dist/images/svg/air.svg" class="svg"><h6>Appliance repair</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/heart.svg" class="svg"><h6>Beauty & Spa</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/roll.svg" class="svg"><h6>Home cleaning & repairs</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/suitcase.svg" class="svg"><h6>Business & taxes</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/automobile.svg" class="svg"><h6>personal & more</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/rings.svg" class="svg"><h6>Wedding & events</h6></a>
					</div>
				</div>
				<div class="tab-pane fade" id="wedding_event" role="tabpanel">
					<div class="list-of-services">
						<a href="#" class="service-provide"><img src="dist/images/svg/air.svg" class="svg"><h6>Appliance repair</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/heart.svg" class="svg"><h6>Beauty & Spa</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/roll.svg" class="svg"><h6>Home cleaning & repairs</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/suitcase.svg" class="svg"><h6>Business & taxes</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/automobile.svg" class="svg"><h6>personal & more</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/rings.svg" class="svg"><h6>Wedding & events</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/air.svg" class="svg"><h6>Appliance repair</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/heart.svg" class="svg"><h6>Beauty & Spa</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/roll.svg" class="svg"><h6>Home cleaning & repairs</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/suitcase.svg" class="svg"><h6>Business & taxes</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/automobile.svg" class="svg"><h6>personal & more</h6></a>
						<a href="#" class="service-provide"><img src="dist/images/svg/rings.svg" class="svg"><h6>Wedding & events</h6></a>
					</div>
				</div>
		    </div>
		  </div>
 	</div>
  </div>
</div>