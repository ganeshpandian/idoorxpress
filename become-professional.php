<?php include 'header-loggedin.php'; ?>
  <div class="container-fluid become-proff">
    <div class="page-comer container">
      Home / Listingpage / Become Proffetional
    </div>
    <div class="container serv-buss">
      <div class="col-md-8">
        <h3>Expand your service <br> business with Service Cambodia</h3>
        <p>Join a community of <b>65,000+</b> professionals who have successfully grown their business with <b>Service Cambodia</b>.</p>
        <ul>
          <li>
            <b>3.0 Million +</b>
            <p>Verified customer requests on <br>our platform</p>
          </li>
          <li>
            <b>100,000 +</b>
            <p>Professionals earning on our <br>platform</p>
          </li>
          <li>
            <b>100 +</b>
            <p>Services across various <br>categories</p>
          </li>
        </ul>
      </div>
      <div class="col-md-4">
        <h4>Become an Service Cambodia Partner within minutes</h4>
        <label>
          <select class="form-control" id="exampleFormControlSelect1">
            <option>Choose your service</option>
            <option>Appliance Repair</option>
            <option>Home Cleaning</option>
            <option>Shifting Homes</option>
            <option>Painting & Renovation</option>
            <option>Wedding Services</option>
          </select>
        </label>
        <ul>
          <li class="col-md-6"><input type="text" class="form-control" placeholder="First Name"></li>
          <li class="col-md-6"><input type="text" class="form-control" placeholder="Last Name"></li>
          <li class="col-md-3"><input type="text" class="form-control" value="+855"></li>
          <li class="col-md-9"><input type="text" class="form-control" placeholder="Mobile Number"></li>
          <li class="col-md-12"><input type="password" class="form-control" placeholder="password"></li>
        </ul>
        <input type="submit" name="" value="signup">
      </div>
    </div>
  </div>

  <div class="container app-steps">
    <div class="col-md-4 ph-img">
      <figure><img src="dist/images/app.png"></figure>
    </div>
    <div class="col-md-8 step-three">
      <ul>
        <li>
          <i>1</i>
          <b>Get verified customer requests</b>
          <p>Get connected with customers looking for your service.</p>
        </li>
        <li>
          <i>2</i>
          <b>Pay to send quotes</b>
          <p>Pay only to reply to customer leads that match your interests and qualifications.</p>
        </li>
        <li>
          <i>3</i>
          <b>Chat and get hired</b>
          <p>Chat with the customers, finalise the quotes and get hired. Know More.</p>
        </li>
      </ul>
    </div>
  </div>
  <div class="container-fluid service-special">
    <div class="container fav-service">
      <div class="col-md-7 dance-deta">
        <figure><img src="dist/images/zumba.png"></figure>
        <p>Aastha joined Service Cambodia 6 months ago and has been <b>hired more than 45 times</b> till now. She uses the app regularly to get potential clients in Delhi NCR and has managed to successfully establish and grow her business with <b>Service Cambodia</b>.</p>
        <hr>
        <h3>Aastha Sharma</h3>
        <span>Hired 45 times</span>
        <small>Bollywood Dance Instructor</small>
      </div>
      <div class="col-md-5 form-signup">
        <p>Take a look at how many customers request for professionals like you</p>
        <label>
          <select class="form-control" id="exampleFormControlSelect1">
            <option>Choose your service</option>
            <option>Appliance Repair</option>
            <option>Home Cleaning</option>
            <option>Shifting Homes</option>
            <option>Painting & Renovation</option>
            <option>Wedding Services</option>
          </select>
        </label>
        <input type="submit" name="" value="signup">
      </div>
    </div>
  </div>
  <div class="container why-partner">
    <h3>Why become an Service Cambodia Partner?</h3>
    <ul>
      <li><i class="icon-aircraft"></i><h4>Grow your business</h4><p>Get new customers looking for service professionals like you.</p></li>
      <li><i class="icon-users2"></i><h4>Work on your own terms</h4><p>Quote your own price and choose the customer requests that interest you.</p></li>
      <li><i class="icon-book-open"></i><h4>Business Tools</h4><p>Get business tools like online payment and invoicing.</p></li>
    </ul>
  </div>
  <div class="container-fluid trusted-by">
    <div class="container owner-list">
      <h2>Trusted By</h2>
      <ul>
        <li>
          <figure><img src="dist/images/photographer.jpg"></figure>
          <h3>Aravinth Sakthivel</h3>
          <span>Wedding Photographer</span>
          <hr>
          <p>From earning 10K a month to being totally booked from November till February <br> - My Service Cambodia journey.</p>
        </li>
        <li>
          <figure><img src="dist/images/photographer.jpg"></figure>
          <h3>Aravinth Sakthivel</h3>
          <span>Wedding Photographer</span>
          <hr>
          <p>From earning 10K a month to being totally booked from November till February <br> - My Service Cambodia journey.</p>
        </li>
        <li>
          <figure><img src="dist/images/photographer.jpg"></figure>
          <h3>Aravinth Sakthivel</h3>
          <span>Wedding Photographer</span>
          <hr>
          <p>From earning 10K a month to being totally booked from November till February <br> - My Service Cambodia journey.</p>
        </li>
      </ul>
    </div>
  </div>
  <div class="container prof-videos">
    <h3>Featured Professionals</h3>
    <ul>
      <li data-toggle="modal" data-target="#video_popup"><figure><img src="dist/images/video-thumb.jpg"></figure><i class="icon-controller-play"></i></li>
      <li data-toggle="modal" data-target="#video_popup"><figure><img src="dist/images/video-thumb.jpg"></figure><i class="icon-controller-play"></i></li>
      <li data-toggle="modal" data-target="#video_popup"><figure><img src="dist/images/video-thumb.jpg"></figure><i class="icon-controller-play"></i></li>
    </ul>
  </div>

<?php include 'footer.php'; ?>

<div class="modal fade" id="video_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <iframe width="100%" height="650" src="https://www.youtube.com/embed/Sg0wZrKWrlM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>
</div>