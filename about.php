<?php include 'header.php'; ?>

<div class="container-fluid section-abt">
	<div class="container about-us">
		<div class="abt-sev">
			<h3>What is Sevacam?</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt pulvinar magna, non porttitor nunc semper a. Ut eget porta metus. Maecenas vitae turpis sed arcu consequat consectetur vitae in justo. Vestibulum aliquet egestas elementum. Nulla tortor augue, tempor vitae dapibus vel, dignissim at dui. Nulla facilisi. Morbi maximus luctus tempor. Vivamus eu convallis nisi. Aenean suscipit lectus id risus luctus laoreet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Cras quis nunc suscipit nunc condimentum ultrices interdum ut enim. Ut erat orci, tristique vitae mi vitae, pellentesque vehicula justo. Etiam faucibus libero hendrerit, venenatis mauris in, hendrerit massa. Etiam porta eros eu nulla sagittis, et commodo odio pulvinar. Integer tempus nulla vel leo posuere, tincidunt posuere urna posuere. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt pulvinar magna, non porttitor nunc semper a. Ut eget porta metus. Maecenas vitae turpis sed arcu consequat consectetur vitae in justo. Vestibulum aliquet egestas elementum. Nulla tortor augue, tempor vitae dapibus vel, dignissim at dui. Nulla facilisi. Morbi maximus luctus tempor. Vivamus eu convallis nisi. Aenean suscipit lectus id risus luctus laoreet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Cras quis nunc suscipit nunc condimentum ultrices interdum ut enim. Ut erat orci, tristique vitae mi vitae, pellentesque vehicula justo. Etiam faucibus libero hendrerit, venenatis mauris in, hendrerit massa. Etiam porta eros eu nulla sagittis, et commodo odio pulvinar. Integer tempus nulla vel leo posuere, tincidunt posuere urna posuere. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt pulvinar magna, non porttitor nunc semper a. Ut eget porta metus. Maecenas vitae turpis sed arcu consequat consectetur vitae in justo. Vestibulum aliquet egestas elementum. Nulla tortor augue, tempor vitae dapibus vel, dignissim at dui. Nulla facilisi. Morbi maximus luctus tempor. Vivamus eu convallis nisi. Aenean suscipit lectus id risus luctus laoreet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Cras quis nunc suscipit nunc condimentum ultrices interdum ut enim. Ut erat orci, tristique vitae mi vitae, pellentesque vehicula justo. Etiam faucibus libero hendrerit, venenatis mauris in, hendrerit massa. Etiam porta eros eu nulla sagittis, et commodo odio pulvinar. Integer tempus nulla vel leo posuere, tincidunt posuere urna posuere. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
		</div>		
	</div>
</div>
<div class="container-fluid trusted-by">
	<div class="container owner-list">
	  <h2>Trusted By</h2>
	  <ul>
	    <li>
	      <figure><img src="dist/images/photographer.jpg"></figure>
	      <h3>Aravinth Sakthivel</h3>
	      <span>Wedding Photographer</span>
	      <hr>
	      <p>From earning 10K a month to being totally booked from November till February <br> - My Service Cambodia journey.</p>
	    </li>
	    <li>
	      <figure><img src="dist/images/photographer.jpg"></figure>
	      <h3>Aravinth Sakthivel</h3>
	      <span>Wedding Photographer</span>
	      <hr>
	      <p>From earning 10K a month to being totally booked from November till February <br> - My Service Cambodia journey.</p>
	    </li>
	    <li>
	      <figure><img src="dist/images/photographer.jpg"></figure>
	      <h3>Aravinth Sakthivel</h3>
	      <span>Wedding Photographer</span>
	      <hr>
	      <p>From earning 10K a month to being totally booked from November till February <br> - My Service Cambodia journey.</p>
	    </li>
	  </ul>
	</div>
</div>
<div class="container-fluid find-investors" style="background: url(dist/images/investors.jpg);background-size: cover;background-attachment: fixed;">
	<div class="container invest-area">
		<h3>Our Investors</h3>
		<ul>
			<li>
				<figure><img src="dist/images/photographer.jpg"></figure>
				<div class="name-fetch">
					<h4>Aravinth Sakthivel</h4>
					<h5>Business Man</h5>
				</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt pulvinar magna, non porttitor nunc semper a. Ut eget porta metus. Maecenas vitae turpis sed arcu consequat consectetur vitae in justo. Vestibulum aliquet egestas elementum. Nulla tortor augue, tempor vitae dapibus vel, dignissim at dui..</p>
			</li>
			<li>
				<figure><img src="dist/images/photographer.jpg"></figure>
				<div class="name-fetch">
					<h4>Aravinth Sakthivel</h4>
					<h5>Business Man</h5>
				</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt pulvinar magna, non porttitor nunc semper a. Ut eget porta metus. Maecenas vitae turpis sed arcu consequat consectetur vitae in justo. Vestibulum aliquet egestas elementum. Nulla tortor augue, tempor vitae dapibus vel, dignissim at dui..</p>
			</li>
		</ul>
	</div>
</div>
<div class="container-fluid add-footernew">
	<div class="container">
		<h3>RELATED SERVICES IN DELHI</h3>
		<ul>
			<li><a href="#">Architect</a></li>
			<li><a href="#">Carpenter</a></li>
			<li><a href="#">Construction and Renovation</a></li>
			<li><a href="#">Electrician</a></li>
			<li><a href="#">Interior Designer</a></li>
			<li><a href="#">Massage for Men</a></li>
			<li><a href="#">Packers & Movers</a></li>
			<li><a href="#">Hairstyling & Makeup</a></li>
			<li><a href="#">Pest Control</a></li>
			<li><a href="#">Plumber</a></li>
			<li><a href="#">Bathroom Deep Cleaning</a></li>
			<li><a href="#">Home Deep Cleaning</a></li>
			<li><a href="#">Kitchen Deep Cleaning</a></li>
			<li><a href="#">Sofa Cleaning</a></li>
			<li><a href="#">RO or Water Purifier Repair</a></li>
			<li><a href="#">Salon at Home</a></li>
			<li><a href="#">Spa at Home for Women</a></li>
		</ul>
	</div>
</div>
<?php include 'footer.php'; ?>