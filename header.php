<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="dist/images/fav.png">
	
	<link rel="stylesheet" href="dist/styles/vendor.css?v=0.0.1">
	<link rel="stylesheet" href="dist/scripts/plugins/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="dist/styles/style.css?v=0.0.1">
	
	
	<script src="dist/scripts/plugins/vendor.js" type="text/javascript"></script>
	<script src="dist/scripts/plugins/OwlCarousel2-2.3.4/dist/owl.carousel.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
<script type="text/javascript"> 
  $(window).on('load', function() { // makes sure the whole site is loaded  
  $('#status').fadeOut(); // will first fade out the loading animation  
  $('#preloader').delay(100).fadeOut('slow'); // will fade out the white DIV that covers the website.  
  $('body').delay(100).css({'overflow':'visible'}); 
}) 
</script> 

<style type="text/css"> 
  #preloader { 
  position: fixed; 
  top: 0; 
  left: 0; 
  right: 0; 
  bottom: 0; 
  background-color: #fff; 
  /* change if the mask should have another color then white */ 
  z-index: 99999999; 
  /* makes sure it stays on top */ 
} 
 
#status {
  position: absolute;
  left: 50%;
  top: 15%;
  background: url(dist/images/Spinner.gif);
  background-repeat: no-repeat;
  background-position: center;
  margin: -100px 0 0 -100px;
  width: 250px;
  background-size: 100%;
  height: 100%;
}
</style> 
</head>
<body>
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <header>
    <div class="header-section">
      <div class="homepagecontainer container">
        <div class="col-md-3 col-sm-4 col-xs-12 logo">
          <a class="logo" href="/">
            <img src="./dist/images/logo.png" alt="idoorxpress" />
          </a>
        </div>
        <div class="col-md-9 col-sm-8 col-xs-12 login">
          <ul class="list-inline pull-right">
            <li class="language dropdown"><a href="#" id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">En <i class="icon-chevron-down"></i></a>
              <ul class="dropdown-menu" aria-labelledby="dLabel">
                <li><a href="#"><img src="dist/images/en.png">en</a></li>
                <li><a href="#"><img src="dist/images/ch.png">zh</a></li>
                <li><a href="#"><img src="dist/images/kh.png">km</a></li>
                
              </ul>
            </li>
            <li class="language dropdown"><a href="#" id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">USD <i class="icon-chevron-down"></i></a>
              <ul class="dropdown-menu" aria-labelledby="dLabel">
                <li><a href="#">USD</a></li>
                <li><a href="#">EUR</a></li>
                <li><a href="#">INR</a></li>
                
              </ul>
            </li>
            <li class="login_a"><a href="#" data-toggle="modal" data-target="#login_popup">Login</a></li>
            <li><a href="#" class="theme_btn">Become a Agent</a></li>
          </ul>
        </div>
      </div>
    </div>
  </header>
  <!-- Login Modal -->
<div class="login-transit">
  <div class="modal fade" id="login_popup" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"><i class="icon-lock2"></i>Account Login</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <i class="icon-circle-with-cross"></i>
          </button>
        </div>
        <div class="modal-body">
          <span class="fb"><a href="#"><img src="dist/images/fb.png"> Login with facebook</a></span>
          <span class="gg"><a href="#"><img src="dist/images/gg.png"> Login with Google</span>
          <hr>
          <form>
            <ul>
              <li class="col-md-3"><input type="text" class="form-control" value="+855"></li>
              <li class="col-md-9"><input type="text" class="form-control" placeholder="Mobile Number"></li>
            </ul>
            <div class="login-passy">
              <input type="password" class="form-control" placeholder="Password" id="login_pass">
              <span id="password-field"> <i class="fa fa-eye-slash"></i></span>
            </div>
            <button type="button">Login in</button>
          </form>
          <div class="logib-reg">
            <a href="#" class="forget-passy">Forget password?</a>
            Don’t have an account? <a href="#" data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target="#register_tab">Register</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Register Modal -->
<div class="modal fade" id="register_tab" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="icon-lock2"></i>Account Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="icon-circle-with-cross"></i>
        </button>
      </div>
      <div class="modal-body">
        <span class="fb"><a href="#"><img src="dist/images/fb.png"> Continue with facebook</a></span>
        <span class="gg"><a href="#"><img src="dist/images/gg.png"> Continue with Google</a></span>
        <hr>
        <form>
          <input type="text" class="form-control" placeholder="First Name">
          <input type="text" class="form-control" placeholder="Last Name">
          <ul>
            <li class="col-md-3"><input type="text" class="form-control" value="+855"></li>
            <li class="col-md-9"><input type="text" class="form-control" placeholder="Mobile Number"></li>
          </ul>
          <div class="login-passy">
              <input type="password" class="form-control" placeholder="Password" id="login_pass1">
              <span id="password-field1"> <i class="fa fa-eye-slash"></i></span>
            </div>
          <button type="button">Create Account</button>
        </form>
        <div class="logib-reg">
          Already have an account <a href="#" data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target="#login_popup">Log In</a>
        </div>
      </div>
    </div>
  </div>
</div>
  