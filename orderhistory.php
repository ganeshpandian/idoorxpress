<?php include 'header.php'; ?>
<div class="prof-banner">

</div>
<div class="add-addr ord-history">
    <h2>Order History</h2>
    <div class="check-history no-order">
        <img src="dist/images/no-results.png" alt="">
        <h3>You haven't booked any service</h3>
        <p>There is no booking found on order history. Start searching services now</p>
        <a class="theme_btn">Back to Home Page</a>
    </div>
    <div class="check-history">
        <ul>
            <li><img src="dist/images/repair-3.jpg" alt=""></li>
            <li>
                <h4> <i class="icon-user"></i> Aravinth Sakthivel</h4>
                <h3>Pick up address</h3>
                <p>no 21 street 330 phnom penh Cambodia 3000, no 21 street 330 phnom penh Cambodia 3000, no 21 street 330 phnom penh Cambodia 3000</p>
                <a href="#">+855 900909090</a>
            </li>
            <button class="pull-right">Track Order</button>
            <span>Order in Progress</span>
        </ul>
    </div>
    <div class="check-history">
        <ul>
            <li><img src="dist/images/repair-3.jpg" alt=""></li>
            <li>
                <h4> <i class="icon-user"></i> Aravinth Sakthivel</h4>
                <h3>Pick up address</h3>
                <p>no 21 street 330 phnom penh Cambodia 3000, no 21 street 330 phnom penh Cambodia 3000, no 21 street 330 phnom penh Cambodia 3000</p>
                <a href="#">+855 900909090</a>
            </li>
            <button class="pull-right">Track Order</button>
            <span>Order in Progress</span>
        </ul>
    </div>
</div>
<?php include 'footer.php'; ?>